import {observableTest} from "./observableTest";

console.log('in main.ts');
const someObj = new System.Object();
console.log('main.ts: someObj:',someObj);

const fooBar = new Foo.Bar();
console.log('instantiated Foo.Bar: fooBar:',fooBar);


console.log('in main.ts: 0');
const dataManager = new Typescript.Gen.Test.DataManager();
console.log('in main.ts: 1');
const dataObj = dataManager.GetData();
console.log('in main.ts: 2');

console.log('main.ts: dataObj:', dataObj);

observableTest();
