
export function observableTest() {

	// JINT does not properly handle "value tuples" - so the following will fail with error:
	// JintVm.Execute: ex: Javascript.Jint.JintJsException: Filepath: main No public methods with the specified arguments were found.
	/*
	const fancyObservable = new Typescript.Gen.Test.FancyObservable();
	//fancyObservable.Subscribe((one:any, two:string, three:string) =>
	fancyObservable.Subscribe((obj:[one:System.Collections.Generic.List<string>, two:string, three:string]) =>
	{
		//console.log("observableTest: neat: Subscribe: updatedData: " + updatedData.entities.Count + " and: " + updatedData.cmd + " and: " + updatedData.data);
		//console.log("observableTest: neat: Subscribe: one:",one,"two:",two,"three:",three);
		console.log("observableTest: neat: Subscribe: one:",obj[0],"two:",obj[1],"three:",obj[2]);
	});
	*/

	// test that the Observer logs the events from the Observable
	console.log('observableTest: before calling constructor');
	new Typescript.Gen.Test.ObservableTest();
	console.log('observableTest: after calling constructor');


	const nameObservable: Typescript.Gen.Test.NameObservable = new Typescript.Gen.Test.NameObservable();
	const nameObserver: Typescript.Gen.Test.NameObserver = new Typescript.Gen.Test.NameObserver();
	// TPC: you can pass the observable to the observer or vice-versa - unirx likes to pass observable to observable
	//nameObserver.Subscribe(nameObservable);
	nameObservable.Subscribe(nameObserver);

	//TPC: not sure why this callback function signature no longer works
	let sub = nameObservable.Subscribe((name) => {
		console.log('observableTest: in UniRx Subscribe extension: updatedName:', name);
	});

	nameObservable.UpdateName("testing");
	nameObservable.CommitName();

	console.log('observableTest: after test');
}

