using UniRedux;
using static UniRedux.Selectors;

public static class TestSelectors
{
	public static ISelectorWithoutProps<TestState, uint?> SelectTestUint = CreateSelector(
		(TestState state) => (state.TestUint)
	);

	public static ISelectorWithoutProps<TestState, int?> SelectTestInt = CreateSelector(
		(TestState state) => (state.TestInt)
	);

	public static ISelectorWithoutProps<TestState, float> SelectTestFloat = CreateSelector(
		(TestState state) => (state.TestFloat)
	);

	public static ISelectorWithoutProps<TestState, long?> SelectTestLong = CreateSelector(
		(TestState state) => (state.TestLong)
	);

	public static TestEntities<TestState> TestEntitiesConnector = TestEntities<TestState>.Create(state => state.TestEntities, (state, entities) => { state.TestEntities = entities; return state; });
}
