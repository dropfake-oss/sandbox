using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FixedPointTesting
{
	[Serializable]
	[StructLayout(LayoutKind.Explicit)]
	public struct FP : IEquatable<FP>, IComparable<FP>
	{
		public static class Raw
		{
			public const long SmallestNonZero = 1L;

			public const long MinValue = long.MinValue;

			public const long MaxValue = long.MaxValue;

			public const long Pi = 205887L;

			public const long _0 = 0L;

			public const long _1 = 65536L;

			public const long _2 = 131072L;

			public const long _3 = 196608L;

			public const long _4 = 262144L;

			public const long _5 = 327680L;

			public const long _10 = 655360L;

			public const long _100 = 6553600L;

			public const long Minus_1 = -65536L;

			public const long Rad_90 = 102943L;
		}

		public class Comparer : IComparer<FP>
		{
			public static readonly Comparer Instance = new Comparer();

			private Comparer()
			{
			}

			int IComparer<FP>.Compare(FP x, FP y)
			{
				return x.RawValue.CompareTo(y.RawValue);
			}
		}

		public class EqualityComparer : IEqualityComparer<FP>
		{
			public static readonly EqualityComparer Instance = new EqualityComparer();

			private EqualityComparer()
			{
			}

			bool IEqualityComparer<FP>.Equals(FP x, FP y)
			{
				return x.RawValue == y.RawValue;
			}

			int IEqualityComparer<FP>.GetHashCode(FP num)
			{
				return num.RawValue.GetHashCode();
			}
		}

		public const int SIZE = 8;

		private const int FRACTIONS_COUNT = 5;

		public const long RAW_ONE = 65536L;

		public const long RAW_ZERO = 0L;

		public const int Precision = 16;

		public const int Bits = 64;

		public const long MulRound = 0L;

		public const int MulShift = 16;

		public const int MulShiftTrunc = 16;

		internal const bool UsesRoundedConstants = false;

		[FieldOffset(0)]
		public long RawValue;

		public unsafe static FP SmallestNonZero
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 1L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP MinValue
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = long.MinValue;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP MaxValue
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = long.MaxValue;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP Pi
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 205887L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP _0
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 0L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP _1
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 65536L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP _2
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 131072L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP _3
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 196608L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP _4
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 262144L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP _5
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 327680L;
				return *(FP*)(&num);
			}
		}


		public unsafe static FP _10
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 655360L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP _100
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 6553600L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP Minus_1
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = -65536L;
				return *(FP*)(&num);
			}
		}

		public unsafe static FP Rad_90
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				long num = 102943L;
				return *(FP*)(&num);
			}
		}

		public long AsLong
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				return RawValue >> 16;
			}
		}

		public int AsInt
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				return (int)(RawValue >> 16);
			}
		}

		public short AsShort
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				return (short)(RawValue >> 16);
			}
		}

		public float AsFloat
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				return (float)RawValue / 65536f;
			}
		}

		public double AsDouble
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				return (double)RawValue / 65536.0;
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator -(FP a)
		{
			a.RawValue = -a.RawValue;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator +(FP a)
		{
			a.RawValue = a.RawValue;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator +(FP a, FP b)
		{
			a.RawValue += b.RawValue;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator +(FP a, int b)
		{
			a.RawValue += (long)b << 16;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator +(int a, FP b)
		{
			b.RawValue = ((long)a << 16) + b.RawValue;
			return b;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator -(FP a, FP b)
		{
			a.RawValue -= b.RawValue;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator -(FP a, int b)
		{
			a.RawValue -= (long)b << 16;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator -(int a, FP b)
		{
			b.RawValue = ((long)a << 16) - b.RawValue;
			return b;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator *(FP a, FP b)
		{
			a.RawValue = a.RawValue * b.RawValue >> 16;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator *(FP a, int b)
		{
			a.RawValue *= b;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator *(int a, FP b)
		{
			b.RawValue = a * b.RawValue;
			return b;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator /(FP a, FP b)
		{
			a.RawValue = (a.RawValue << 16) / b.RawValue;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator /(FP a, int b)
		{
			a.RawValue /= b;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator /(int a, FP b)
		{
			b.RawValue = ((long)a << 32) / b.RawValue;
			return b;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator %(FP a, FP b)
		{
			a.RawValue %= b.RawValue;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator %(FP a, int b)
		{
			a.RawValue %= (long)b << 16;
			return a;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP operator %(int a, FP b)
		{
			b.RawValue = ((long)a << 16) % b.RawValue;
			return b;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator <(FP a, FP b)
		{
			return a.RawValue < b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator <(FP a, int b)
		{
			return a.RawValue < (long)b << 16;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator <(int a, FP b)
		{
			return (long)a << 16 < b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator <=(FP a, FP b)
		{
			return a.RawValue <= b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator <=(FP a, int b)
		{
			return a.RawValue <= (long)b << 16;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator <=(int a, FP b)
		{
			return (long)a << 16 <= b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator >(FP a, FP b)
		{
			return a.RawValue > b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator >(FP a, int b)
		{
			return a.RawValue > (long)b << 16;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator >(int a, FP b)
		{
			return (long)a << 16 > b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator >=(FP a, FP b)
		{
			return a.RawValue >= b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator >=(FP a, int b)
		{
			return a.RawValue >= (long)b << 16;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator >=(int a, FP b)
		{
			return (long)a << 16 >= b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator ==(FP a, FP b)
		{
			return a.RawValue == b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator ==(FP a, int b)
		{
			return a.RawValue == (long)b << 16;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator ==(int a, FP b)
		{
			return (long)a << 16 == b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator !=(FP a, FP b)
		{
			return a.RawValue != b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator !=(FP a, int b)
		{
			return a.RawValue != (long)b << 16;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator !=(int a, FP b)
		{
			return (long)a << 16 != b.RawValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator FP(int value)
		{
			FP result = default(FP);
			result.RawValue = (long)value << 16;
			return result;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator FP(uint value)
		{
			FP result = default(FP);
			result.RawValue = (long)((ulong)value << 16);
			return result;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator FP(short value)
		{
			FP result = default(FP);
			result.RawValue = (long)value << 16;
			return result;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator FP(ushort value)
		{
			FP result = default(FP);
			result.RawValue = (long)((ulong)value << 16);
			return result;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator FP(sbyte value)
		{
			FP result = default(FP);
			result.RawValue = (long)value << 16;
			return result;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator FP(byte value)
		{
			FP result = default(FP);
			result.RawValue = (long)((ulong)value << 16);
			return result;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator int(FP value)
		{
			return (int)(value.RawValue >> 16);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator long(FP value)
		{
			return value.RawValue >> 16;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator float(FP value)
		{
			return (float)value.RawValue / 65536f;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator double(FP value)
		{
			return (double)value.RawValue / 65536.0;
		}

		[Obsolete("Don't cast from float to FP", true)]
		public static implicit operator FP(float value)
		{
			throw new InvalidOperationException();
		}

		[Obsolete("Don't cast from double to FP", true)]
		public static implicit operator FP(double value)
		{
			throw new InvalidOperationException();
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		internal FP(long v)
		{
			RawValue = v;
		}

		public int CompareTo(FP other)
		{
			return RawValue.CompareTo(other.RawValue);
		}

		public bool Equals(FP other)
		{
			return RawValue == other.RawValue;
		}

		public override bool Equals(object obj)
		{
			if (obj is FP)
			{
				return RawValue == ((FP)obj).RawValue;
			}
			return false;
		}

		public override int GetHashCode()
		{
			return RawValue.GetHashCode();
		}

		public override string ToString()
		{
			return AsFloat.ToString(CultureInfo.InvariantCulture);
		}

		public string ToString(string format)
		{
			return AsDouble.ToString(format, CultureInfo.InvariantCulture);
		}

		public string ToStringInternal()
		{
			long num = Math.Abs(RawValue);
			string text = $"{num >> 16}.{(num % 65536).ToString(CultureInfo.InvariantCulture).PadLeft(5, '0')}";
			if (RawValue < 0)
			{
				return "-" + text;
			}
			return text;
		}

		public static FP FromFloat_UNSAFE(float value)
		{
			return new FP(checked((long)(value * 65536f)));
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP FromRaw(long value)
		{
			FP result = default(FP);
			result.RawValue = value;
			return result;
		}

		public static FP FromString_UNSAFE(string value)
		{
			return FromFloat_UNSAFE((float)double.Parse(value, CultureInfo.InvariantCulture));
		}

		public static FP FromString(string value)
		{
			if (value == null)
			{
				return _0;
			}
			value = value.Trim();
			if (value.Length == 0)
			{
				return _0;
			}
			bool flag = false;
			bool flag2 = value[0] == '.';
			if (flag = value[0] == '-')
			{
				value = value.Substring(1);
			}
			string[] array = value.Split(new char[1] { '.' }, StringSplitOptions.RemoveEmptyEntries);
			long num = 0L;
			num = array.Length switch
			{
				1 => (!flag2) ? ParseInteger(array[0]) : ParseFractions(array[0]),
				2 => checked(ParseInteger(array[0]) + ParseFractions(array[1])),
				_ => throw new FormatException(value),
			};
			if (flag)
			{
				return new FP(-num);
			}
			return new FP(num);
		}

		private static long ParseInteger(string format)
		{
			return long.Parse(format) * 65536;
		}

		private static long ParseFractions(string format)
		{
			long num;
			switch (format.Length)
			{
				case 0:
					return 0L;
				case 1:
					num = 10L;
					break;
				case 2:
					num = 100L;
					break;
				case 3:
					num = 1000L;
					break;
				case 4:
					num = 10000L;
					break;
				case 5:
					num = 100000L;
					break;
				case 6:
					num = 1000000L;
					break;
				case 7:
					num = 10000000L;
					break;
				default:
					{
						if (format.Length > 14)
						{
							format = format.Substring(0, 14);
						}
						num = 100000000L;
						for (int i = 8; i < format.Length; i++)
						{
							num *= 10;
						}
						break;
					}
			}
			long num2 = long.Parse(format);
			return (num2 * 65536 + num / 2) / num;
		}

		internal static long RawMultiply(FP x, FP y)
		{
			return x.RawValue * y.RawValue >> 16;
		}

		internal static long RawMultiply(FP x, FP y, FP z)
		{
			y.RawValue = x.RawValue * y.RawValue >> 16;
			return y.RawValue * z.RawValue >> 16;
		}

		internal static long RawMultiply(FP x, FP y, FP z, FP a)
		{
			y.RawValue = x.RawValue * y.RawValue >> 16;
			z.RawValue = y.RawValue * z.RawValue >> 16;
			return z.RawValue * a.RawValue >> 16;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static FP MulTruncate(FP x, FP y)
		{
			return FromRaw(x.RawValue * y.RawValue >> 16);
		}
	}
}
