using System.Collections.Generic;
using System;
using UnityEngine;
using Javascript.Core;
using Javascript.Jint;
using Javascript.Unity;
using FixedPointTesting;

namespace Testing
{
	public class TestReflect
	{
		public void Log(string text)
		{
			Debug.Log("TestReflect: Log: " + text);
		}

		public static void Log2(string text)
		{
			Debug.Log("TestReflect: Log: " + text);
		}
	}
}

public class ScriptingBackend : Javascript.Core.IFileSystem
{
	private IJavascriptVirtualMachine _jintVm;

	// Unity API doesn't expose a "File Exists" for Resources - so we've created a file cache for when we're just checking if a file exists
	private Dictionary<string, byte[]> _jintFileCache = new Dictionary<string, byte[]>();

	private JintRunner _jintRunner;

	private const int _numMillis = 10;

	public ScriptingBackend()
	{
	}

	public void Init()
	{
		// Give JINT permission to bind to namespaces in rts.core assembly
		var logger = new UnityJavascriptLogger();
		var console = new UnityJavascriptConsole();

		var assemblyNamespacesForCodegen = SetupTypescriptCodegen.GetAssemblyNamespaces();

		var assembly = typeof(Testing.TestReflect).Assembly;
		var executingAssemblyNamespaces = new AssemblyNamespaces(assembly, new string[] { "Testing", "Foo", "Typescript", "FixedPointTesting" });

		var assemblyNamespaces = new List<AssemblyNamespaces>() { executingAssemblyNamespaces };
		assemblyNamespaces.AddRange(assemblyNamespacesForCodegen);

		var extensionMethodsTypes = new Type[] { typeof(UniRx.ObservableExtensions) };
		_jintRunner = new JintRunner(logger, this, assemblyNamespaces, console, null, extensionMethodsTypes);
		_jintVm = _jintRunner.CreateJavascriptVirtualMachine("sandbox");

		_jintVm.UnhandledException += JintUnhandledExceptionHandler;

		TestReferenceToBoundClasses();
	}

	private void TestReferenceToBoundClasses()
	{
		_jintVm.EvalInMem("initJint.js", @"
			console.log('TestReferenceToBoundClasses: 1');
			const testReflect = new Testing.TestReflect();
			//const testReflect = new Testing.TestReflect();

			console.log('TestReferenceToBoundClasses: 2');
			testReflect.Log('looky looky');
			console.log('TestReferenceToBoundClasses: 3');

			Testing.TestReflect.Log2('testy testy');
			console.log('TestReferenceToBoundClasses: 4');

			const foo = new Foo.Bar();
			console.log('TestReferenceToBoundClasses: 5');

			const vec3 = new FixedPointMath.FVector3(0, 0, 0);
			console.log('vec3:', vec3);
		");
	}

	private void JintUnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
	{
		if (e == null)
		{
			throw new System.Exception("JintUnhandledExceptionHandler: UnhandledExceptionEventArgs is null!");
		}
		var exceptionObj = e.ExceptionObject;
		if (exceptionObj == null)
		{
			throw new System.Exception("JintUnhandledExceptionHandler: UnhandledExceptionEventArgs.ExceptionObject is null");
		}
		Exception exception = exceptionObj as Exception;
		if (exception == null)
		{
			throw new System.Exception("JintUnhandledExceptionHandler: UnhandledExceptionEventArgs.ExceptionObject is not of type Exception!");
		}
		throw exception;
	}

	public struct HitResult
	{
		public int AttackType;
		public FP Damage;
	}

	public void RunScript(string scriptFile)
	{
		try
		{
			//Debug.Log("ScriptingBackend: RunScript: before JintVm.EvalSource: script: " + scriptFile);
			//UnityEngine.Debug.Log("ScriptingBackend: RunScript: after JintVm.EvalSource: script: " + script + " result: " + result);
			_jintVm.Eval(scriptFile);
		}
		catch (Exception ex)
		{
			Debug.LogError("ScriptingBackend: RunScript: JintVm.Execute: ex: " + ex);
			throw;
		}

		var lines = new string[]
		{
			"let adjustment = delta + FixedPointTesting.FP._5;",
			"switch(hit.AttackType) {",
			"	case 1:",
			"		adjustment = delta + FixedPointTesting.FP._5;",
			"		break;",
			"	case 2:",
			"		adjustment = delta + FixedPointTesting.FP._5;",
			"		break;",
			"	default:",
			"		break;",
			"}",
			"console.log('Hit Type:' + hit.AttackType + ' Damage:' + hit.Damage.ToString() + ' Adjustment: ' + adjustment.ToString());",
			"return adjustment;",
		};

		var hit = new HitResult();
		hit.AttackType = 2;
		hit.Damage = FP._4;

		var delta = FP._100;
		var output = _jintVm.RegisterAndInvokeFunction("foobar", SourceUtils.LinesToSourceCode(lines), "delta, hit", delta, hit);
		var adjustment = _jintVm.AsStruct<FP>(output);
		UnityEngine.Debug.Log("RegisterAndInvokeFunction adjustment: " + adjustment);
	}

	public void SimUpdate()
	{
		_jintRunner.Update(_numMillis);
	}

	byte[] GetFileBytesFromUnity(string path)
	{
		var fullPath = System.IO.Path.Combine("scripts", path.Replace(".js", ""));
		// TPC: the following annoyance (replace backslash with forward slash) is because on Windows - if you use Path.Combine(..) it obviously will use "\" - so we need to make the path "consistent" (all forward slashes)
		fullPath = fullPath.Replace("\\", "/");
		//Debug.Log("ScriptingBackend: GetFileBytesFromUnity: path: " + path + " fullPath: " + fullPath);

		// Our js paths use "/" - so on Windows - we can't use System.IO.Path.DirectorySeparatorChar
		fullPath = Javascript.Core.PathUtils.ExtractPath(fullPath, '/');
		fullPath = fullPath.Replace('/', System.IO.Path.DirectorySeparatorChar);
		byte[] fileBytes;
		if (_jintFileCache.TryGetValue(fullPath, out fileBytes))
		{
			if (fileBytes == null)
			{
				_jintFileCache.Remove(fullPath);
				throw new System.Exception("ScriptingBackend: GetFileBytesFromUnity: JintFileCache polluted with a null entry at: " + fullPath);
			}
			return fileBytes;
		}
		//Debug.Log("ScriptingBackend: GetFileBytesFromUnity: fullPath: " + fullPath);
		var ta = UnityEngine.Resources.Load<TextAsset>(fullPath);
		//Debug.Log("ScriptingBackend: GetFileBytesFromUnity: (ta != null): " + (ta != null) + " for: " + fullPath);
		if (ta == null) // TPC: is this actually how Unity's API works?  That is - it doesn't just throw an exception if the file isn't found?
		{
			return null;
		}
		else
		{
			fileBytes = ta.bytes;
			_jintFileCache[fullPath] = fileBytes;
			return fileBytes;
		}
	}

	bool Javascript.Core.IFileSystem.Exists(string path)
	{
		try
		{
			byte[] fileBytes = GetFileBytesFromUnity(path);
			if (fileBytes == null)
			{
				return false;
			}
			return true;
		}
		catch (System.Exception ex)
		{
			Debug.LogError("ScriptingBackend: Exists: path: " + path + " exception: " + ex);
			return false;
		}
	}

	public byte[] ReadAllBytes(string path)
	{
		try
		{
			byte[] fileBytes = GetFileBytesFromUnity(path);
			if (fileBytes == null)
			{
				throw new System.IO.FileNotFoundException("ScriptingBackend: ReadAllBytes: null file bytes for file: " + path);
			}
			return fileBytes;
		}
		catch (Exception ex)
		{
			Debug.LogError("ScriptingBackend: Exists: path: " + path + " exception: " + ex);
			throw;
		}
	}

	public void WriteFile(string path, string contents)
	{
		throw new NotImplementedException("do we need to support WriteFile in this context (path: " + path + "!");
	}

	public void WriteFile(string path, byte[] bytes)
	{
		throw new NotImplementedException("do we need to support WriteFile in this context (path: " + path + "!");
	}
}
