"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observableTest_1 = require("./observableTest");
console.log('in main.ts');
var someObj = new System.Object();
console.log('main.ts: someObj:', someObj);
var fooBar = new Foo.Bar();
console.log('instantiated Foo.Bar: fooBar:', fooBar);
console.log('in main.ts: 0');
var dataManager = new Typescript.Gen.Test.DataManager();
console.log('in main.ts: 1');
var dataObj = dataManager.GetData();
console.log('in main.ts: 2');
console.log('main.ts: dataObj:', dataObj);
(0, observableTest_1.observableTest)();
//# sourceMappingURL=main.js.map