// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableUpdateSelectedTrigger
declare namespace UniRx.Triggers {
	class ObservableUpdateSelectedTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnUpdateSelectedAsObservable(): System.IObservable<any>
	}
}
