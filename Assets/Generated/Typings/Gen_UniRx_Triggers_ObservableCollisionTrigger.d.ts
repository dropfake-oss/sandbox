// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableCollisionTrigger
declare namespace UniRx.Triggers {
	class ObservableCollisionTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnCollisionEnterAsObservable(): System.IObservable<any>
		OnCollisionExitAsObservable(): System.IObservable<any>
		OnCollisionStayAsObservable(): System.IObservable<any>
	}
}
