// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableUpdateTrigger
declare namespace UniRx.Triggers {
	class ObservableUpdateTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		UpdateAsObservable(): System.IObservable<UniRx.Unit>
	}
}
