// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableEndDragTrigger
declare namespace UniRx.Triggers {
	class ObservableEndDragTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnEndDragAsObservable(): System.IObservable<any>
	}
}
