// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ISchedulerQueueing
declare namespace UniRx {
	interface ISchedulerQueueing {
		ScheduleQueueing<T>(cancel: UniRx.ICancelable, state: T, action: ((obj: T) => void)): void
	}
}
