// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: FixedPointMath
// Type: FixedPointMath.FVector2
declare namespace FixedPointMath {
	class FVector2 extends System.Object {
		x: FixedPointMath.Fixed
		y: FixedPointMath.Fixed
		static readonly zero: FixedPointMath.FVector2
		static readonly one: FixedPointMath.FVector2
		static readonly right: FixedPointMath.FVector2
		static readonly left: FixedPointMath.FVector2
		static readonly up: FixedPointMath.FVector2
		static readonly down: FixedPointMath.FVector2
		readonly magnitude: FixedPointMath.Fixed
		readonly normalized: FixedPointMath.FVector2
		constructor(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed)
		constructor(value: FixedPointMath.Fixed)
		Set(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed): void
		static Reflect(vector: any, normal: any, result: any): void
		static Reflect(vector: FixedPointMath.FVector2, normal: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Add(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Add(value1: any, value2: any, result: any): void
		static Barycentric(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2, value3: FixedPointMath.FVector2, amount1: FixedPointMath.Fixed, amount2: FixedPointMath.Fixed): FixedPointMath.FVector2
		static Barycentric(value1: any, value2: any, value3: any, amount1: FixedPointMath.Fixed, amount2: FixedPointMath.Fixed, result: any): void
		static CatmullRom(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2, value3: FixedPointMath.FVector2, value4: FixedPointMath.FVector2, amount: FixedPointMath.Fixed): FixedPointMath.FVector2
		static CatmullRom(value1: any, value2: any, value3: any, value4: any, amount: FixedPointMath.Fixed, result: any): void
		static Clamp(value1: FixedPointMath.FVector2, min: FixedPointMath.FVector2, max: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Clamp(value1: any, min: any, max: any, result: any): void
		static Distance(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.Fixed
		static Distance(value1: any, value2: any, result: any): void
		static DistanceSquared(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.Fixed
		static DistanceSquared(value1: any, value2: any, result: any): void
		static Divide(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Divide(value1: any, value2: any, result: any): void
		static Divide(value1: FixedPointMath.FVector2, divider: FixedPointMath.Fixed): FixedPointMath.FVector2
		static Divide(value1: any, divider: FixedPointMath.Fixed, result: any): void
		static Dot(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.Fixed
		static Dot(value1: any, value2: any, result: any): void
		Equals(obj: System.Object): boolean
		Equals(other: FixedPointMath.FVector2): boolean
		GetHashCode(): number
		static Hermite(value1: FixedPointMath.FVector2, tangent1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2, tangent2: FixedPointMath.FVector2, amount: FixedPointMath.Fixed): FixedPointMath.FVector2
		static Hermite(value1: any, tangent1: any, value2: any, tangent2: any, amount: FixedPointMath.Fixed, result: any): void
		static ClampMagnitude(vector: FixedPointMath.FVector2, maxLength: FixedPointMath.Fixed): FixedPointMath.FVector2
		LengthSquared(): FixedPointMath.Fixed
		static Lerp(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2, amount: FixedPointMath.Fixed): FixedPointMath.FVector2
		static LerpUnclamped(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2, amount: FixedPointMath.Fixed): FixedPointMath.FVector2
		static LerpUnclamped(value1: any, value2: any, amount: FixedPointMath.Fixed, result: any): void
		static Max(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Max(value1: any, value2: any, result: any): void
		static Min(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Min(value1: any, value2: any, result: any): void
		Scale(other: FixedPointMath.FVector2): void
		static Scale(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Multiply(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Multiply(value1: FixedPointMath.FVector2, scaleFactor: FixedPointMath.Fixed): FixedPointMath.FVector2
		static Multiply(value1: any, scaleFactor: FixedPointMath.Fixed, result: any): void
		static Multiply(value1: any, value2: any, result: any): void
		static Negate(value: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Negate(value: any, result: any): void
		Normalize(): void
		static Normalize(value: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Normalize(value: any, result: any): void
		static SmoothStep(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2, amount: FixedPointMath.Fixed): FixedPointMath.FVector2
		static SmoothStep(value1: any, value2: any, amount: FixedPointMath.Fixed, result: any): void
		static Subtract(value1: FixedPointMath.FVector2, value2: FixedPointMath.FVector2): FixedPointMath.FVector2
		static Subtract(value1: any, value2: any, result: any): void
		static Angle(a: FixedPointMath.FVector2, b: FixedPointMath.FVector2): FixedPointMath.Fixed
		ToFVector(): FixedPointMath.FVector3
		ToString(): string
	}
}
