// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableTransformChangedTrigger
declare namespace UniRx.Triggers {
	class ObservableTransformChangedTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnBeforeTransformParentChangedAsObservable(): System.IObservable<UniRx.Unit>
		OnTransformParentChangedAsObservable(): System.IObservable<UniRx.Unit>
		OnTransformChildrenChangedAsObservable(): System.IObservable<UniRx.Unit>
	}
}
