// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: Assembly-CSharp
// Type: Typescript.Gen.Test.DataManager
declare namespace Typescript.Gen.Test {
	class DataManager extends System.Object {
		constructor()
		GetData(): System.Object
		GetNumberOfSystems(): number
	}
}
