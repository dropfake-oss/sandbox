// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IReadOnlyReactiveCollection`1
declare namespace UniRx {
	interface IReadOnlyReactiveCollection<T> {
		readonly Count: number
		ObserveAdd(): System.IObservable<UniRx.CollectionAddEvent<T>>
		ObserveCountChanged(notifyCurrentCount: boolean): System.IObservable<number>
		ObserveMove(): System.IObservable<UniRx.CollectionMoveEvent<T>>
		ObserveRemove(): System.IObservable<UniRx.CollectionRemoveEvent<T>>
		ObserveReplace(): System.IObservable<UniRx.CollectionReplaceEvent<T>>
		ObserveReset(): System.IObservable<UniRx.Unit>
	}
}
