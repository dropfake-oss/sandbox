// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservablePointerExitTrigger
declare namespace UniRx.Triggers {
	class ObservablePointerExitTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnPointerExitAsObservable(): System.IObservable<any>
	}
}
