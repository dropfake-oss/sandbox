// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableStateMachineTrigger
declare namespace UniRx.Triggers {
	class ObservableStateMachineTrigger extends UnityEngine.Object {
		constructor()
		OnStateExit(animator: any, stateInfo: any, layerIndex: number): void
		OnStateExitAsObservable(): System.IObservable<any>
		OnStateEnter(animator: any, stateInfo: any, layerIndex: number): void
		OnStateEnterAsObservable(): System.IObservable<any>
		OnStateIK(animator: any, stateInfo: any, layerIndex: number): void
		OnStateIKAsObservable(): System.IObservable<any>
		OnStateUpdate(animator: any, stateInfo: any, layerIndex: number): void
		OnStateUpdateAsObservable(): System.IObservable<any>
		OnStateMachineEnter(animator: any, stateMachinePathHash: number): void
		OnStateMachineEnterAsObservable(): System.IObservable<any>
		OnStateMachineExit(animator: any, stateMachinePathHash: number): void
		OnStateMachineExitAsObservable(): System.IObservable<any>
	}
}
