// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableFixedUpdateTrigger
declare namespace UniRx.Triggers {
	class ObservableFixedUpdateTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		FixedUpdateAsObservable(): System.IObservable<UniRx.Unit>
	}
}
