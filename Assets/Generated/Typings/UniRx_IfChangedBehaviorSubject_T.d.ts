// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IfChangedBehaviorSubject`1
declare namespace UniRx {
	class IfChangedBehaviorSubject<T> extends UniRx.BehaviorSubject<T> implements UniRx.ISubject<T, T>, UniRx.ISubject<T>, System.IObservable<T>, System.IObserver<T>, UniRx.IOptimizedObservable<T>, System.IDisposable {
		constructor(defaultValue: T)
		OnNext(value: T): void
	}
}
