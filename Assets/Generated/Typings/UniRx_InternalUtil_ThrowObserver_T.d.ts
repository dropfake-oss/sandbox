// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.InternalUtil.ThrowObserver`1
declare namespace UniRx.InternalUtil {
	class ThrowObserver<T> extends System.Object implements System.IObserver<T> {
		static readonly Instance: UniRx.InternalUtil.ThrowObserver<T>
		OnCompleted(): void
		OnError(error: System.Exception): void
		OnNext(value: T): void
		Synchronize(): System.IObserver<T>
		Synchronize(gate: System.Object): System.IObserver<T>
	}
}
