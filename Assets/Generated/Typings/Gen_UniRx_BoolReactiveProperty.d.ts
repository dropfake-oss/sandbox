// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.BoolReactiveProperty
declare namespace UniRx {
	class BoolReactiveProperty extends UniRx.ReactiveProperty<boolean> implements System.IObservable<boolean>, UniRx.IReadOnlyReactiveProperty<boolean>, UniRx.IReactiveProperty<boolean>, UniRx.IOptimizedObservable<boolean>, System.IDisposable {
		constructor()
		constructor(initialValue: boolean)
	}
}
