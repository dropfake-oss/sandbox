// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableEventTrigger
declare namespace UniRx.Triggers {
	class ObservableEventTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnDeselectAsObservable(): System.IObservable<any>
		OnMoveAsObservable(): System.IObservable<any>
		OnPointerDownAsObservable(): System.IObservable<any>
		OnPointerEnterAsObservable(): System.IObservable<any>
		OnPointerExitAsObservable(): System.IObservable<any>
		OnPointerUpAsObservable(): System.IObservable<any>
		OnSelectAsObservable(): System.IObservable<any>
		OnPointerClickAsObservable(): System.IObservable<any>
		OnSubmitAsObservable(): System.IObservable<any>
		OnDragAsObservable(): System.IObservable<any>
		OnBeginDragAsObservable(): System.IObservable<any>
		OnEndDragAsObservable(): System.IObservable<any>
		OnDropAsObservable(): System.IObservable<any>
		OnUpdateSelectedAsObservable(): System.IObservable<any>
		OnInitializePotentialDragAsObservable(): System.IObservable<any>
		OnCancelAsObservable(): System.IObservable<any>
		OnScrollAsObservable(): System.IObservable<any>
	}
}
