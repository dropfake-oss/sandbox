// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.MultilineReactivePropertyAttribute
declare namespace UniRx {
	class MultilineReactivePropertyAttribute extends System.Object {
		readonly Lines: number
		constructor()
		constructor(lines: number)
	}
}
