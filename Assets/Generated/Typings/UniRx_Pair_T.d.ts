// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Pair`1
declare namespace UniRx {
	class Pair<T> extends System.Object {
		readonly Previous: T
		readonly Current: T
		constructor(previous: T, current: T)
		GetHashCode(): number
		Equals(obj: System.Object): boolean
		Equals(other: UniRx.Pair<T>): boolean
		ToString(): string
	}
}
