// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IAsyncMessagePublisher
declare namespace UniRx {
	interface IAsyncMessagePublisher {
		PublishAsync<T>(message: T): System.IObservable<UniRx.Unit>
	}
}
