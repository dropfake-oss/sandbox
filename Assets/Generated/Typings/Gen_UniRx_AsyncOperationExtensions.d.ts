// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.AsyncOperationExtensions
declare namespace UniRx {
	abstract class AsyncOperationExtensions extends System.Object {
		static AsObservable(asyncOperation: any, progress: any): System.IObservable<any>
		static AsAsyncOperationObservable<T>(asyncOperation: T, progress: any): System.IObservable<T>
	}
}
