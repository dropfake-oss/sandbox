// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Unit
declare namespace UniRx {
	class Unit extends System.Object {
		static readonly Default: UniRx.Unit
		Equals(other: UniRx.Unit): boolean
		Equals(obj: System.Object): boolean
		GetHashCode(): number
		ToString(): string
	}
}
