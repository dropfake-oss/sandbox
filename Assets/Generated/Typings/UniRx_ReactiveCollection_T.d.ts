// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ReactiveCollection`1
declare namespace UniRx {
	class ReactiveCollection<T> extends System.Object implements UniRx.IReadOnlyReactiveCollection<T>, UniRx.IReactiveCollection<T>, System.IDisposable {
		constructor()
		constructor(collection: any)
		constructor(list: System.Collections.Generic.List<T>)
		Move(oldIndex: number, newIndex: number): void
		ObserveCountChanged(notifyCurrentCount: boolean): System.IObservable<number>
		ObserveReset(): System.IObservable<UniRx.Unit>
		ObserveAdd(): System.IObservable<UniRx.CollectionAddEvent<T>>
		ObserveMove(): System.IObservable<UniRx.CollectionMoveEvent<T>>
		ObserveRemove(): System.IObservable<UniRx.CollectionRemoveEvent<T>>
		ObserveReplace(): System.IObservable<UniRx.CollectionReplaceEvent<T>>
		Dispose(): void
	}
}
