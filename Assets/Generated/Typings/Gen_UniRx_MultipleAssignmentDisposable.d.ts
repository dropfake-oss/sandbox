// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.MultipleAssignmentDisposable
declare namespace UniRx {
	class MultipleAssignmentDisposable extends System.Object implements UniRx.ICancelable, System.IDisposable {
		readonly IsDisposed: boolean
		Disposable: System.IDisposable
		constructor()
		Dispose(): void
	}
}
