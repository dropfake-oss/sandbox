// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableCanvasGroupChangedTrigger
declare namespace UniRx.Triggers {
	class ObservableCanvasGroupChangedTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnCanvasGroupChangedAsObservable(): System.IObservable<UniRx.Unit>
	}
}
