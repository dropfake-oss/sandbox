// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ISchedulerPeriodic
declare namespace UniRx {
	interface ISchedulerPeriodic {
		SchedulePeriodic(period: any, action: (() => void)): System.IDisposable
	}
}
