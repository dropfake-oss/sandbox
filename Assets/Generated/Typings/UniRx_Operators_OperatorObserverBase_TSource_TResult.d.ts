// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Operators.OperatorObserverBase`2
declare namespace UniRx.Operators {
	abstract class OperatorObserverBase<TSource, TResult> extends System.Object implements System.IObserver<TSource>, System.IDisposable {
		OnNext(value: TSource): void
		OnError(error: System.Exception): void
		OnCompleted(): void
		Dispose(): void
		Synchronize(): System.IObserver<TSource>
		Synchronize(gate: System.Object): System.IObserver<TSource>
	}
}
