// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: Assembly-CSharp
// Type: Lord.Dance.IAction
declare namespace Lord.Dance {
	interface IAction extends Rabbit.Fudge.IEffect, Hooligan.Foo.IBehaviour {
	}
}
