// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.AsyncReactiveCommandExtensions
declare namespace UniRx {
	abstract class AsyncReactiveCommandExtensions extends System.Object {
		static ToAsyncReactiveCommand(sharedCanExecuteSource: UniRx.IReactiveProperty<boolean>): UniRx.AsyncReactiveCommand
		static ToAsyncReactiveCommand<T>(sharedCanExecuteSource: UniRx.IReactiveProperty<boolean>): UniRx.AsyncReactiveCommand<T>
		static WaitUntilExecuteAsync<T>(source: UniRx.IAsyncReactiveCommand<T>, cancellationToken: any): any
		static GetAwaiter<T>(command: UniRx.IAsyncReactiveCommand<T>): any
		static BindTo(command: UniRx.IAsyncReactiveCommand<UniRx.Unit>, button: any): System.IDisposable
		static BindToOnClick(command: UniRx.IAsyncReactiveCommand<UniRx.Unit>, button: any, asyncOnClick: ((arg: UniRx.Unit) => System.IObservable<UniRx.Unit>)): System.IDisposable
		static BindToOnClick(button: any, asyncOnClick: ((arg: UniRx.Unit) => System.IObservable<UniRx.Unit>)): System.IDisposable
		static BindToOnClick(button: any, sharedCanExecuteSource: UniRx.IReactiveProperty<boolean>, asyncOnClick: ((arg: UniRx.Unit) => System.IObservable<UniRx.Unit>)): System.IDisposable
	}
}
