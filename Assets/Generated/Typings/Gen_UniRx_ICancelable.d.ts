// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ICancelable
declare namespace UniRx {
	interface ICancelable extends System.IDisposable {
		readonly IsDisposed: boolean
	}
}
