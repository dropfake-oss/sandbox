// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableTriggerBase
declare namespace UniRx.Triggers {
	abstract class ObservableTriggerBase extends UnityEngine.Object {
		AwakeAsObservable(): System.IObservable<UniRx.Unit>
		StartAsObservable(): System.IObservable<UniRx.Unit>
		OnDestroyAsObservable(): System.IObservable<UniRx.Unit>
	}
}
