// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IMessageReceiver
declare namespace UniRx {
	interface IMessageReceiver {
		Receive<T>(): System.IObservable<T>
	}
}
