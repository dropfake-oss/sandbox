// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Diagnostics.LogEntry
declare namespace UniRx.Diagnostics {
	class LogEntry extends System.Object {
		readonly LoggerName: string
		readonly LogType: any
		readonly Message: string
		readonly Timestamp: any
		readonly Context: UnityEngine.Object
		readonly Exception: System.Exception
		readonly StackTrace: string
		readonly State: System.Object
		constructor(loggerName: string, logType: any, timestamp: any, message: string, context: UnityEngine.Object, exception: System.Exception, stackTrace: string, state: System.Object)
		ToString(): string
	}
}
