// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableCollision2DTrigger
declare namespace UniRx.Triggers {
	class ObservableCollision2DTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnCollisionEnter2DAsObservable(): System.IObservable<any>
		OnCollisionExit2DAsObservable(): System.IObservable<any>
		OnCollisionStay2DAsObservable(): System.IObservable<any>
	}
}
