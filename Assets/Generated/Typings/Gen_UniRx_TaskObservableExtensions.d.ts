// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.TaskObservableExtensions
declare namespace UniRx {
	abstract class TaskObservableExtensions extends System.Object {
		static ToObservable(task: any): System.IObservable<UniRx.Unit>
		static ToObservable(task: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Unit>
		static ToObservable<TResult>(task: any): System.IObservable<TResult>
		static ToObservable<TResult>(task: any, scheduler: UniRx.IScheduler): System.IObservable<TResult>
		static ToTask<TResult>(observable: System.IObservable<TResult>): any
		static ToTask<TResult>(observable: System.IObservable<TResult>, state: System.Object): any
		static ToTask<TResult>(observable: System.IObservable<TResult>, cancellationToken: any): any
		static ToTask<TResult>(observable: System.IObservable<TResult>, cancellationToken: any, state: System.Object): any
	}
}
