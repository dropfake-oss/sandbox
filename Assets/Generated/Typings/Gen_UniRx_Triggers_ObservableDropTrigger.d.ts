// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableDropTrigger
declare namespace UniRx.Triggers {
	class ObservableDropTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnDropAsObservable(): System.IObservable<any>
	}
}
