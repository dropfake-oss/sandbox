// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.SerialDisposable
declare namespace UniRx {
	class SerialDisposable extends System.Object implements UniRx.ICancelable, System.IDisposable {
		readonly IsDisposed: boolean
		Disposable: System.IDisposable
		constructor()
		Dispose(): void
	}
}
