// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ReactiveCommandExtensions
declare namespace UniRx {
	abstract class ReactiveCommandExtensions extends System.Object {
		static ToReactiveCommand(canExecuteSource: System.IObservable<boolean>, initialValue: boolean): UniRx.ReactiveCommand
		static ToReactiveCommand<T>(canExecuteSource: System.IObservable<boolean>, initialValue: boolean): UniRx.ReactiveCommand<T>
		static WaitUntilExecuteAsync<T>(source: UniRx.IReactiveCommand<T>, cancellationToken: any): any
		static GetAwaiter<T>(command: UniRx.IReactiveCommand<T>): any
		static BindTo(command: UniRx.IReactiveCommand<UniRx.Unit>, button: any): System.IDisposable
		static BindToOnClick(command: UniRx.IReactiveCommand<UniRx.Unit>, button: any, onClick: ((obj: UniRx.Unit) => void)): System.IDisposable
		static BindToButtonOnClick(canExecuteSource: System.IObservable<boolean>, button: any, onClick: ((obj: UniRx.Unit) => void), initialValue: boolean): System.IDisposable
	}
}
