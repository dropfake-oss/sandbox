// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ReplaySubject`1
declare namespace UniRx {
	class ReplaySubject<T> extends System.Object implements UniRx.ISubject<T, T>, UniRx.ISubject<T>, System.IObservable<T>, System.IObserver<T>, UniRx.IOptimizedObservable<T>, System.IDisposable {
		constructor()
		constructor(scheduler: UniRx.IScheduler)
		constructor(bufferSize: number)
		constructor(bufferSize: number, scheduler: UniRx.IScheduler)
		constructor(window: any)
		constructor(window: any, scheduler: UniRx.IScheduler)
		constructor(bufferSize: number, window: any, scheduler: UniRx.IScheduler)
		OnCompleted(): void
		OnError(error: System.Exception): void
		OnNext(value: T): void
		Subscribe(observer: System.IObserver<T>): System.IDisposable
		Dispose(): void
		IsRequiredSubscribeOnCurrentThread(): boolean
	}
}
