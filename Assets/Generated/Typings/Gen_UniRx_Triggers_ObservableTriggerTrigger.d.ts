// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableTriggerTrigger
declare namespace UniRx.Triggers {
	class ObservableTriggerTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnTriggerEnterAsObservable(): System.IObservable<any>
		OnTriggerExitAsObservable(): System.IObservable<any>
		OnTriggerStayAsObservable(): System.IObservable<any>
	}
}
