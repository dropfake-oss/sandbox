// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: mscorlib
// Type: System.Collections.Generic.List`1
declare namespace System.Collections.Generic {
	class List<T> extends Array {
		[index: number]: T;
		Capacity: number
		readonly Count: number
		constructor()
		constructor(capacity: number)
		constructor(collection: any)
		Add(item: T): void
		AddRange(collection: any): void
		AsReadOnly(): any
		BinarySearch(index: number, count: number, item: T, comparer: any): number
		BinarySearch(item: T): number
		BinarySearch(item: T, comparer: any): number
		Clear(): void
		Contains(item: T): boolean
		ConvertAll<TOutput>(converter: ((input: T) => TOutput)): System.Collections.Generic.List<TOutput>
		CopyTo(array: T[]): void
		CopyTo(index: number, array: T[], arrayIndex: number, count: number): void
		CopyTo(array: T[], arrayIndex: number): void
		Exists(match: ((obj: T) => boolean)): boolean
		Find(match: ((obj: T) => boolean)): T
		FindAll(match: ((obj: T) => boolean)): System.Collections.Generic.List<T>
		FindIndex(match: ((obj: T) => boolean)): number
		FindIndex(startIndex: number, match: ((obj: T) => boolean)): number
		FindIndex(startIndex: number, count: number, match: ((obj: T) => boolean)): number
		FindLast(match: ((obj: T) => boolean)): T
		FindLastIndex(match: ((obj: T) => boolean)): number
		FindLastIndex(startIndex: number, match: ((obj: T) => boolean)): number
		FindLastIndex(startIndex: number, count: number, match: ((obj: T) => boolean)): number
		ForEach(action: ((obj: T) => void)): void
		GetEnumerator(): any
		GetRange(index: number, count: number): System.Collections.Generic.List<T>
		IndexOf(item: T): number
		IndexOf(item: T, index: number): number
		IndexOf(item: T, index: number, count: number): number
		Insert(index: number, item: T): void
		InsertRange(index: number, collection: any): void
		LastIndexOf(item: T): number
		LastIndexOf(item: T, index: number): number
		LastIndexOf(item: T, index: number, count: number): number
		Remove(item: T): boolean
		RemoveAll(match: ((obj: T) => boolean)): number
		RemoveAt(index: number): void
		RemoveRange(index: number, count: number): void
		Reverse(): void
		Reverse(index: number, count: number): void
		Sort(): void
		Sort(comparer: any): void
		Sort(index: number, count: number, comparer: any): void
		Sort(comparison: ((x: T, y: T) => number)): void
		ToArray(): T[]
		TrimExcess(): void
		TrueForAll(match: ((obj: T) => boolean)): boolean
		Free(): void
		EraseBack(item: T): boolean
		FindInstanceID(target: T): number
		Contains(value: T, comparer: any): boolean
		IndexOfReference(item: T): number
		FastReverse(): void
	}
}
