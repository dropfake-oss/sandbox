// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ISchedulerLongRunning
declare namespace UniRx {
	interface ISchedulerLongRunning {
		ScheduleLongRunning(action: ((obj: UniRx.ICancelable) => void)): System.IDisposable
	}
}
