// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.DictionaryDisposable`2
declare namespace UniRx {
	class DictionaryDisposable<TKey, TValue> extends Array implements System.IDisposable {
		readonly Count: number
		readonly Keys: any
		readonly Values: any
		constructor()
		constructor(comparer: any)
		Add(key: TKey, value: TValue): void
		Clear(): void
		Remove(key: TKey): boolean
		ContainsKey(key: TKey): boolean
		TryGetValue(key: TKey, value: any): boolean
		GetEnumerator(): any
		GetObjectData(info: any, context: any): void
		OnDeserialization(sender: System.Object): void
		Dispose(): void
	}
}
