// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: FixedPointMath
// Type: FixedPointMath.Fixed
declare namespace FixedPointMath {
	class Fixed extends System.Object {
		_serializedValue: number
		static readonly MAX_VALUE: number
		static readonly MIN_VALUE: number
		static readonly NUM_BITS: number
		static readonly FRACTIONAL_PLACES: number
		static readonly ONE: number
		static readonly TEN: number
		static readonly HALF: number
		static readonly PI_TIMES_2: number
		static readonly PI: number
		static readonly PI_OVER_2: number
		static readonly LUT_SIZE: number
		static readonly Precision: any
		static readonly MaxValue: FixedPointMath.Fixed
		static readonly MinValue: FixedPointMath.Fixed
		static readonly One: FixedPointMath.Fixed
		static readonly Ten: FixedPointMath.Fixed
		static readonly Half: FixedPointMath.Fixed
		static readonly Zero: FixedPointMath.Fixed
		static readonly PositiveInfinity: FixedPointMath.Fixed
		static readonly NegativeInfinity: FixedPointMath.Fixed
		static readonly NaN: FixedPointMath.Fixed
		static readonly EN1: FixedPointMath.Fixed
		static readonly EN2: FixedPointMath.Fixed
		static readonly EN3: FixedPointMath.Fixed
		static readonly EN4: FixedPointMath.Fixed
		static readonly EN5: FixedPointMath.Fixed
		static readonly EN6: FixedPointMath.Fixed
		static readonly EN7: FixedPointMath.Fixed
		static readonly EN8: FixedPointMath.Fixed
		static readonly Epsilon: FixedPointMath.Fixed
		static readonly Pi: FixedPointMath.Fixed
		static readonly PiOver2: FixedPointMath.Fixed
		static readonly PiTimes2: FixedPointMath.Fixed
		static readonly PiInv: FixedPointMath.Fixed
		static readonly PiOver2Inv: FixedPointMath.Fixed
		static readonly Deg2Rad: FixedPointMath.Fixed
		static readonly Rad2Deg: FixedPointMath.Fixed
		static readonly LutInterval: FixedPointMath.Fixed
		static readonly AcosLut: number[]
		static readonly SinLut: number[]
		static readonly TanLut: number[]
		readonly RawValue: number
		constructor(value: number)
		static Sign(value: FixedPointMath.Fixed): number
		static Abs(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static FastAbs(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Floor(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Ceiling(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Round(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static OverflowAdd(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed): FixedPointMath.Fixed
		static FastAdd(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed): FixedPointMath.Fixed
		static OverflowSub(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed): FixedPointMath.Fixed
		static FastSub(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed): FixedPointMath.Fixed
		static OverflowMul(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed): FixedPointMath.Fixed
		static FastMul(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed): FixedPointMath.Fixed
		static CountLeadingZeroes(x: number): number
		static FastMod(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Sqrt(x: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Sin(x: FixedPointMath.Fixed): FixedPointMath.Fixed
		static FastSin(x: FixedPointMath.Fixed): FixedPointMath.Fixed
		static ClampSinValue(angle: number, flipHorizontal: any, flipVertical: any): number
		static Cos(x: FixedPointMath.Fixed): FixedPointMath.Fixed
		static FastCos(x: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Tan(x: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Atan(y: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Atan2(y: FixedPointMath.Fixed, x: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Asin(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Acos(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		AsFloat(): number
		AsInt(): number
		AsLong(): number
		AsDouble(): number
		AsDecimal(): any
		static ToFloat(value: FixedPointMath.Fixed): number
		static ToInt(value: FixedPointMath.Fixed): number
		static FromFloat(value: number): FixedPointMath.Fixed
		static IsInfinity(value: FixedPointMath.Fixed): boolean
		static IsNaN(value: FixedPointMath.Fixed): boolean
		Equals(obj: System.Object): boolean
		GetHashCode(): number
		Equals(other: FixedPointMath.Fixed): boolean
		CompareTo(other: FixedPointMath.Fixed): number
		ToString(): string
		static FromRaw(rawValue: number): FixedPointMath.Fixed
	}
}
