// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.UnityUIComponentExtensions
declare namespace UniRx {
	abstract class UnityUIComponentExtensions extends System.Object {
		static SubscribeToText(source: System.IObservable<string>, text: any): System.IDisposable
		static SubscribeToText<T>(source: System.IObservable<T>, text: any): System.IDisposable
		static SubscribeToText<T>(source: System.IObservable<T>, text: any, selector: ((arg: T) => string)): System.IDisposable
		static SubscribeToInteractable(source: System.IObservable<boolean>, selectable: any): System.IDisposable
		static OnClickAsObservable(button: any): System.IObservable<UniRx.Unit>
		static OnValueChangedAsObservable(toggle: any): System.IObservable<boolean>
		static OnValueChangedAsObservable(scrollbar: any): System.IObservable<number>
		static OnValueChangedAsObservable(scrollRect: any): System.IObservable<UnityEngine.Vector2>
		static OnValueChangedAsObservable(slider: any): System.IObservable<number>
		static OnEndEditAsObservable(inputField: any): System.IObservable<string>
		static OnValueChangedAsObservable(inputField: any): System.IObservable<string>
		static OnValueChangedAsObservable(dropdown: any): System.IObservable<number>
	}
}
