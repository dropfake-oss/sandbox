// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableAnimatorTrigger
declare namespace UniRx.Triggers {
	class ObservableAnimatorTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnAnimatorIKAsObservable(): System.IObservable<number>
		OnAnimatorMoveAsObservable(): System.IObservable<UniRx.Unit>
	}
}
