// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservablePointerUpTrigger
declare namespace UniRx.Triggers {
	class ObservablePointerUpTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnPointerUpAsObservable(): System.IObservable<any>
	}
}
