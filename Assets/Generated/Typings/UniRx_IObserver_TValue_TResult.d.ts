// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IObserver`2
declare namespace UniRx {
	interface IObserver<TValue, TResult> {
		OnNext(value: TValue): TResult
		OnError(exception: System.Exception): TResult
		OnCompleted(): TResult
	}
}
