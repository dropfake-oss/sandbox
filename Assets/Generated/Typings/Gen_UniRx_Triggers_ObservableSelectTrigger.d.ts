// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableSelectTrigger
declare namespace UniRx.Triggers {
	class ObservableSelectTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnSelectAsObservable(): System.IObservable<any>
	}
}
