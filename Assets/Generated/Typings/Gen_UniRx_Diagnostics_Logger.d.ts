// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Diagnostics.Logger
declare namespace UniRx.Diagnostics {
	class Logger extends System.Object {
		readonly Name: string
		constructor(loggerName: string)
		Debug(message: System.Object, context: UnityEngine.Object): void
		DebugFormat(format: string, ...args: System.Object[]): void
		Log(message: System.Object, context: UnityEngine.Object): void
		LogFormat(format: string, ...args: System.Object[]): void
		Warning(message: System.Object, context: UnityEngine.Object): void
		WarningFormat(format: string, ...args: System.Object[]): void
		Error(message: System.Object, context: UnityEngine.Object): void
		ErrorFormat(format: string, ...args: System.Object[]): void
		Exception(exception: System.Exception, context: UnityEngine.Object): void
		Raw(logEntry: UniRx.Diagnostics.LogEntry): void
	}
}
