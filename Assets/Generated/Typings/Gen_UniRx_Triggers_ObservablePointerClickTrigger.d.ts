// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservablePointerClickTrigger
declare namespace UniRx.Triggers {
	class ObservablePointerClickTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnPointerClickAsObservable(): System.IObservable<any>
	}
}
