// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.MessageBroker
declare namespace UniRx {
	class MessageBroker extends System.Object implements UniRx.IMessagePublisher, UniRx.IMessageReceiver, System.IDisposable, UniRx.IMessageBroker {
		static readonly Default: UniRx.IMessageBroker
		constructor()
		Publish<T>(message: T): void
		Receive<T>(): System.IObservable<T>
		Dispose(): void
	}
}
