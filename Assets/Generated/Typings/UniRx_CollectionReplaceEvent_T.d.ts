// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.CollectionReplaceEvent`1
declare namespace UniRx {
	class CollectionReplaceEvent<T> extends System.Object {
		readonly Index: number
		readonly OldValue: T
		readonly NewValue: T
		constructor(index: number, oldValue: T, newValue: T)
		ToString(): string
		GetHashCode(): number
		Equals(other: UniRx.CollectionReplaceEvent<T>): boolean
	}
}
