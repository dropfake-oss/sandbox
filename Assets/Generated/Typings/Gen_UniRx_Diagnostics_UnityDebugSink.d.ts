// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Diagnostics.UnityDebugSink
declare namespace UniRx.Diagnostics {
	class UnityDebugSink extends System.Object implements System.IObserver<UniRx.Diagnostics.LogEntry> {
		constructor()
		OnCompleted(): void
		OnError(error: System.Exception): void
		OnNext(value: UniRx.Diagnostics.LogEntry): void
		Synchronize(): System.IObserver<UniRx.Diagnostics.LogEntry>
		Synchronize(gate: System.Object): System.IObserver<UniRx.Diagnostics.LogEntry>
	}
}
