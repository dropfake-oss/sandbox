// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableLateUpdateTrigger
declare namespace UniRx.Triggers {
	class ObservableLateUpdateTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		LateUpdateAsObservable(): System.IObservable<UniRx.Unit>
	}
}
