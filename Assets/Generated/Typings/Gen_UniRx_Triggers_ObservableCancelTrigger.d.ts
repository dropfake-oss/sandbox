// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableCancelTrigger
declare namespace UniRx.Triggers {
	class ObservableCancelTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnCancelAsObservable(): System.IObservable<any>
	}
}
