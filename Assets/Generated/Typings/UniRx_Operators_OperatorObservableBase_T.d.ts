// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Operators.OperatorObservableBase`1
declare namespace UniRx.Operators {
	abstract class OperatorObservableBase<T> extends System.Object implements System.IObservable<T>, UniRx.IOptimizedObservable<T> {
		IsRequiredSubscribeOnCurrentThread(): boolean
		Subscribe(observer: System.IObserver<T>): System.IDisposable
	}
}
