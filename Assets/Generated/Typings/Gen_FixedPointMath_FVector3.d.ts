// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: FixedPointMath
// Type: FixedPointMath.FVector3
declare namespace FixedPointMath {
	class FVector3 extends System.Object {
		x: FixedPointMath.Fixed
		y: FixedPointMath.Fixed
		z: FixedPointMath.Fixed
		static readonly zero: FixedPointMath.FVector3
		static readonly left: FixedPointMath.FVector3
		static readonly right: FixedPointMath.FVector3
		static readonly up: FixedPointMath.FVector3
		static readonly down: FixedPointMath.FVector3
		static readonly back: FixedPointMath.FVector3
		static readonly forward: FixedPointMath.FVector3
		static readonly one: FixedPointMath.FVector3
		static readonly MinValue: FixedPointMath.FVector3
		static readonly MaxValue: FixedPointMath.FVector3
		readonly sqrMagnitude: FixedPointMath.Fixed
		readonly magnitude: FixedPointMath.Fixed
		readonly normalized: FixedPointMath.FVector3
		constructor(x: number, y: number, z: number)
		constructor(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed, z: FixedPointMath.Fixed)
		constructor(xyz: FixedPointMath.Fixed)
		static Abs(other: FixedPointMath.FVector3): FixedPointMath.FVector3
		static ClampMagnitude(vector: FixedPointMath.FVector3, maxLength: FixedPointMath.Fixed): FixedPointMath.FVector3
		Scale(other: FixedPointMath.FVector3): void
		Set(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed, z: FixedPointMath.Fixed): void
		static Lerp(from: FixedPointMath.FVector3, to: FixedPointMath.FVector3, percent: FixedPointMath.Fixed): FixedPointMath.FVector3
		ToString(): string
		Equals(obj: System.Object): boolean
		static Scale(vecA: FixedPointMath.FVector3, vecB: FixedPointMath.FVector3): FixedPointMath.FVector3
		static Min(value1: FixedPointMath.FVector3, value2: FixedPointMath.FVector3): FixedPointMath.FVector3
		static Min(value1: any, value2: any, result: any): void
		static Max(value1: FixedPointMath.FVector3, value2: FixedPointMath.FVector3): FixedPointMath.FVector3
		static Distance(v1: FixedPointMath.FVector3, v2: FixedPointMath.FVector3): FixedPointMath.Fixed
		static DistanceSquared(v1: FixedPointMath.FVector3, v2: FixedPointMath.FVector3): FixedPointMath.Fixed
		static Max(value1: any, value2: any, result: any): void
		MakeZero(): void
		IsZero(): boolean
		IsNearlyZero(): boolean
		static Transform(position: FixedPointMath.FVector3, matrix: FixedPointMath.FMatrix): FixedPointMath.FVector3
		static Transform(position: any, matrix: any, result: any): void
		static TransposedTransform(position: any, matrix: any, result: any): void
		static Dot(vector1: FixedPointMath.FVector3, vector2: FixedPointMath.FVector3): FixedPointMath.Fixed
		static Dot(vector1: any, vector2: any): FixedPointMath.Fixed
		static Add(value1: FixedPointMath.FVector3, value2: FixedPointMath.FVector3): FixedPointMath.FVector3
		static Add(value1: any, value2: any, result: any): void
		static Divide(value1: FixedPointMath.FVector3, scaleFactor: FixedPointMath.Fixed): FixedPointMath.FVector3
		static Divide(value1: any, scaleFactor: FixedPointMath.Fixed, result: any): void
		static Subtract(value1: FixedPointMath.FVector3, value2: FixedPointMath.FVector3): FixedPointMath.FVector3
		static Subtract(value1: any, value2: any, result: any): void
		static Cross(vector1: FixedPointMath.FVector3, vector2: FixedPointMath.FVector3): FixedPointMath.FVector3
		static Cross(vector1: any, vector2: any, result: any): void
		GetHashCode(): number
		Negate(): void
		static Negate(value: FixedPointMath.FVector3): FixedPointMath.FVector3
		static Negate(value: any, result: any): void
		static Normalize(value: FixedPointMath.FVector3): FixedPointMath.FVector3
		Normalize(): void
		static Normalize(value: any, result: any): void
		static Swap(vector1: any, vector2: any): void
		static Multiply(value1: FixedPointMath.FVector3, scaleFactor: FixedPointMath.Fixed): FixedPointMath.FVector3
		static Multiply(value1: any, scaleFactor: FixedPointMath.Fixed, result: any): void
		static Angle(a: FixedPointMath.FVector3, b: FixedPointMath.FVector3): FixedPointMath.Fixed
		ToFVector2(): FixedPointMath.FVector2
	}
}
