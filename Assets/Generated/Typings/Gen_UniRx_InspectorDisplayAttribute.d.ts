// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.InspectorDisplayAttribute
declare namespace UniRx {
	class InspectorDisplayAttribute extends System.Object {
		readonly FieldName: string
		readonly NotifyPropertyChanged: boolean
		constructor(fieldName: string, notifyPropertyChanged: boolean)
	}
}
