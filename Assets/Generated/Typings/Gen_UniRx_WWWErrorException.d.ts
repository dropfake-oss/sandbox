// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.WWWErrorException
declare namespace UniRx {
	class WWWErrorException extends System.Exception {
		readonly RawErrorMessage: string
		readonly HasResponse: boolean
		readonly Text: string
		readonly StatusCode: any
		readonly ResponseHeaders: any
		readonly WWW: any
		constructor(www: any, text: string)
		ToString(): string
	}
}
