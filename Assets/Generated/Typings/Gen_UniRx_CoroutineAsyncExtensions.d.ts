// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.CoroutineAsyncExtensions
declare namespace UniRx {
	abstract class CoroutineAsyncExtensions extends System.Object {
		static GetAwaiter(coroutine: any): UniRx.CoroutineAsyncBridge
	}
}
