// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableVisibleTrigger
declare namespace UniRx.Triggers {
	class ObservableVisibleTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnBecameInvisibleAsObservable(): System.IObservable<UniRx.Unit>
		OnBecameVisibleAsObservable(): System.IObservable<UniRx.Unit>
	}
}
