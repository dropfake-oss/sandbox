// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ScheduledDisposable
declare namespace UniRx {
	class ScheduledDisposable extends System.Object implements UniRx.ICancelable, System.IDisposable {
		readonly Scheduler: UniRx.IScheduler
		readonly Disposable: System.IDisposable
		readonly IsDisposed: boolean
		constructor(scheduler: UniRx.IScheduler, disposable: System.IDisposable)
		Dispose(): void
	}
}
