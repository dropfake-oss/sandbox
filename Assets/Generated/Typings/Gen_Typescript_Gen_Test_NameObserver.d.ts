// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: Assembly-CSharp
// Type: Typescript.Gen.Test.NameObserver
declare namespace Typescript.Gen.Test {
	class NameObserver extends System.Object implements System.IObserver<string> {
		constructor()
		Subscribe(provider: System.IObservable<string>): void
		Unsubscribe(): void
		OnCompleted(): void
		OnError(error: System.Exception): void
		OnNext(value: string): void
		Synchronize(): System.IObserver<string>
		Synchronize(gate: System.Object): System.IObserver<string>
	}
}
