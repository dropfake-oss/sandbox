// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: mscorlib
// Type: System.IDisposable
declare namespace System {
	interface IDisposable {
		Dispose(): void
	}
}
