// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IEventPattern`2
declare namespace UniRx {
	interface IEventPattern<TSender, TEventArgs> {
		readonly Sender: TSender
		readonly EventArgs: TEventArgs
	}
}
