// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.BooleanDisposable
declare namespace UniRx {
	class BooleanDisposable extends System.Object implements UniRx.ICancelable, System.IDisposable {
		readonly IsDisposed: boolean
		constructor()
		Dispose(): void
	}
}
