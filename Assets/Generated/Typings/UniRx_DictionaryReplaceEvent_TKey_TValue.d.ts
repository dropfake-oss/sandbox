// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.DictionaryReplaceEvent`2
declare namespace UniRx {
	class DictionaryReplaceEvent<TKey, TValue> extends System.Object {
		readonly Key: TKey
		readonly OldValue: TValue
		readonly NewValue: TValue
		constructor(key: TKey, oldValue: TValue, newValue: TValue)
		ToString(): string
		GetHashCode(): number
		Equals(other: UniRx.DictionaryReplaceEvent<TKey, TValue>): boolean
	}
}
