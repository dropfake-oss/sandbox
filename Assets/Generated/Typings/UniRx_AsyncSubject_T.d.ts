// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.AsyncSubject`1
declare namespace UniRx {
	class AsyncSubject<T> extends System.Object implements UniRx.ISubject<T, T>, UniRx.ISubject<T>, System.IObservable<T>, System.IObserver<T>, UniRx.IOptimizedObservable<T>, System.IDisposable {
		readonly Value: T
		readonly HasObservers: boolean
		readonly IsCompleted: boolean
		constructor()
		OnCompleted(): void
		OnError(error: System.Exception): void
		OnNext(value: T): void
		Subscribe(observer: System.IObserver<T>): System.IDisposable
		Dispose(): void
		IsRequiredSubscribeOnCurrentThread(): boolean
		GetAwaiter(): UniRx.AsyncSubject<T>
		OnCompleted(continuation: (() => void)): void
		GetResult(): T
	}
}
