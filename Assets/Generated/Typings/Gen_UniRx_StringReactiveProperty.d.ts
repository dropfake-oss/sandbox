// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.StringReactiveProperty
declare namespace UniRx {
	class StringReactiveProperty extends UniRx.ReactiveProperty<string> implements System.IObservable<string>, UniRx.IReadOnlyReactiveProperty<string>, UniRx.IReactiveProperty<string>, UniRx.IOptimizedObservable<string>, System.IDisposable {
		constructor()
		constructor(initialValue: string)
	}
}
