// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableDeselectTrigger
declare namespace UniRx.Triggers {
	class ObservableDeselectTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnDeselectAsObservable(): System.IObservable<any>
	}
}
