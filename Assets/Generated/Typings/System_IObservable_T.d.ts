// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: mscorlib
// Type: System.IObservable`1
declare namespace System {
	interface IObservable<T> {
		Subscribe(observer: System.IObserver<T>): System.IDisposable
		Scan(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Scan(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate), resultSelector: ((arg: TAccumulate) => TResult)): System.IObservable<TResult>
		GetAwaiter(): UniRx.AsyncSubject<TSource>
		GetAwaiter(cancellationToken: any): UniRx.AsyncSubject<TSource>
		Multicast(subject: UniRx.ISubject<T>): UniRx.IConnectableObservable<T>
		Publish(): UniRx.IConnectableObservable<T>
		Publish(initialValue: T): UniRx.IConnectableObservable<T>
		PublishLast(): UniRx.IConnectableObservable<T>
		Replay(): UniRx.IConnectableObservable<T>
		Replay(scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		Replay(bufferSize: number): UniRx.IConnectableObservable<T>
		Replay(bufferSize: number, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		Replay(window: any): UniRx.IConnectableObservable<T>
		Replay(window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		Replay(bufferSize: number, window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		Share(): System.IObservable<T>
		Wait(): T
		Wait(timeout: any): T
		Concat(): System.IObservable<TSource>
		Concat(...seconds: System.IObservable<TSource>[]): System.IObservable<TSource>
		Merge(...seconds: System.IObservable<T>[]): System.IObservable<T>
		Merge(second: System.IObservable<T>, scheduler: UniRx.IScheduler): System.IObservable<T>
		Merge(): System.IObservable<T>
		Merge(maxConcurrent: number): System.IObservable<T>
		Zip(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		CombineLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		ZipLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		Switch(): System.IObservable<T>
		WithLatestFrom(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		StartWith(value: T): System.IObservable<T>
		StartWith(valueFactory: (() => T)): System.IObservable<T>
		StartWith(...values: T[]): System.IObservable<T>
		StartWith(values: any): System.IObservable<T>
		StartWith(scheduler: UniRx.IScheduler, value: T): System.IObservable<T>
		StartWith(scheduler: UniRx.IScheduler, values: any): System.IObservable<T>
		StartWith(scheduler: UniRx.IScheduler, ...values: T[]): System.IObservable<T>
		Synchronize(): System.IObservable<T>
		Synchronize(gate: System.Object): System.IObservable<T>
		ObserveOn(scheduler: UniRx.IScheduler): System.IObservable<T>
		SubscribeOn(scheduler: UniRx.IScheduler): System.IObservable<T>
		DelaySubscription(dueTime: any): System.IObservable<T>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		DelaySubscription(dueTime: any): System.IObservable<T>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		Amb(second: System.IObservable<T>): System.IObservable<T>
		AsObservable(): System.IObservable<T>
		Cast(): System.IObservable<TResult>
		Cast(witness: TResult): System.IObservable<TResult>
		OfType(): System.IObservable<TResult>
		OfType(witness: TResult): System.IObservable<TResult>
		AsUnitObservable(): System.IObservable<UniRx.Unit>
		AsSingleUnitObservable(): System.IObservable<UniRx.Unit>
		Repeat(): System.IObservable<T>
		RepeatSafe(): System.IObservable<T>
		Select(selector: ((arg: T) => TR)): System.IObservable<TR>
		Select(selector: ((arg1: T, arg2: number) => TR)): System.IObservable<TR>
		Where(predicate: ((arg: T) => boolean)): System.IObservable<T>
		Where(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<T>
		ContinueWith(other: System.IObservable<TR>): System.IObservable<TR>
		ContinueWith(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(other: System.IObservable<TR>): System.IObservable<TR>
		SelectMany(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(selector: ((arg1: TSource, arg2: number) => System.IObservable<TResult>)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: T) => System.IObservable<TC>), resultSelector: ((arg1: T, arg2: TC) => TR)): System.IObservable<TR>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => System.IObservable<TCollection>), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		SelectMany(selector: ((arg: TSource) => any)): System.IObservable<TResult>
		SelectMany(selector: ((arg1: TSource, arg2: number) => any)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: TSource) => any), resultSelector: ((arg1: TSource, arg2: TCollection) => TResult)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => any), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		ToArray(): System.IObservable<T[]>
		ToList(): System.IObservable<any>
		Do(observer: System.IObserver<T>): System.IObservable<T>
		Do(onNext: ((obj: T) => void)): System.IObservable<T>
		Do(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void)): System.IObservable<T>
		Do(onNext: ((obj: T) => void), onCompleted: (() => void)): System.IObservable<T>
		Do(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IObservable<T>
		DoOnError(onError: ((obj: System.Exception) => void)): System.IObservable<T>
		DoOnCompleted(onCompleted: (() => void)): System.IObservable<T>
		DoOnTerminate(onTerminate: (() => void)): System.IObservable<T>
		DoOnSubscribe(onSubscribe: (() => void)): System.IObservable<T>
		DoOnCancel(onCancel: (() => void)): System.IObservable<T>
		Materialize(): System.IObservable<UniRx.Notification<T>>
		Dematerialize(): System.IObservable<T>
		DefaultIfEmpty(): System.IObservable<T>
		DefaultIfEmpty(defaultValue: T): System.IObservable<T>
		Distinct(): System.IObservable<TSource>
		Distinct(comparer: any): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey)): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<TSource>
		DistinctUntilChanged(): System.IObservable<T>
		DistinctUntilChanged(comparer: any): System.IObservable<T>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey)): System.IObservable<T>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey), comparer: any): System.IObservable<T>
		IgnoreElements(): System.IObservable<T>
		ForEachAsync(onNext: ((obj: T) => void)): System.IObservable<UniRx.Unit>
		ForEachAsync(onNext: ((arg1: T, arg2: number) => void)): System.IObservable<UniRx.Unit>
		Finally(finallyAction: (() => void)): System.IObservable<T>
		Catch(errorHandler: ((arg: TException) => System.IObservable<T>)): System.IObservable<T>
		CatchIgnore(): System.IObservable<TSource>
		CatchIgnore(errorAction: ((obj: TException) => void)): System.IObservable<TSource>
		Retry(): System.IObservable<TSource>
		Retry(retryCount: number): System.IObservable<TSource>
		OnErrorRetry(): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void)): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any, delayScheduler: UniRx.IScheduler): System.IObservable<TSource>
		Take(count: number): System.IObservable<T>
		Take(duration: any): System.IObservable<T>
		Take(duration: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		TakeWhile(predicate: ((arg: T) => boolean)): System.IObservable<T>
		TakeWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<T>
		TakeUntil(other: System.IObservable<TOther>): System.IObservable<T>
		TakeLast(count: number): System.IObservable<T>
		TakeLast(duration: any): System.IObservable<T>
		TakeLast(duration: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		Skip(count: number): System.IObservable<T>
		Skip(duration: any): System.IObservable<T>
		Skip(duration: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		SkipWhile(predicate: ((arg: T) => boolean)): System.IObservable<T>
		SkipWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<T>
		SkipUntil(other: System.IObservable<TOther>): System.IObservable<T>
		Buffer(count: number): System.IObservable<any>
		Buffer(count: number, skip: number): System.IObservable<any>
		Buffer(timeSpan: any): System.IObservable<any>
		Buffer(timeSpan: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, count: number): System.IObservable<any>
		Buffer(timeSpan: any, count: number, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(windowBoundaries: System.IObservable<TWindowBoundary>): System.IObservable<any>
		Pairwise(): System.IObservable<UniRx.Pair<T>>
		Pairwise(selector: ((arg1: T, arg2: T) => TR)): System.IObservable<TR>
		Last(): System.IObservable<T>
		Last(predicate: ((arg: T) => boolean)): System.IObservable<T>
		LastOrDefault(): System.IObservable<T>
		LastOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<T>
		First(): System.IObservable<T>
		First(predicate: ((arg: T) => boolean)): System.IObservable<T>
		FirstOrDefault(): System.IObservable<T>
		FirstOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<T>
		Single(): System.IObservable<T>
		Single(predicate: ((arg: T) => boolean)): System.IObservable<T>
		SingleOrDefault(): System.IObservable<T>
		SingleOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<T>
		GroupBy(keySelector: ((arg: TSource) => TKey)): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement)): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		Timestamp(): System.IObservable<UniRx.Timestamped<TSource>>
		Timestamp(scheduler: UniRx.IScheduler): System.IObservable<UniRx.Timestamped<TSource>>
		TimeInterval(): System.IObservable<UniRx.TimeInterval<TSource>>
		TimeInterval(scheduler: UniRx.IScheduler): System.IObservable<UniRx.TimeInterval<TSource>>
		Delay(dueTime: any): System.IObservable<T>
		Delay(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Sample(interval: any): System.IObservable<T>
		Sample(interval: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		Throttle(dueTime: any): System.IObservable<TSource>
		Throttle(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		ThrottleFirst(dueTime: any): System.IObservable<TSource>
		ThrottleFirst(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Timeout(dueTime: any): System.IObservable<T>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		Timeout(dueTime: any): System.IObservable<T>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		SelectMany(coroutine: any, publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: (() => any), publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: ((arg: T) => any)): System.IObservable<UniRx.Unit>
		DelayFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		Sample(sampler: System.IObservable<T2>): System.IObservable<T>
		SampleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		ThrottleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		ThrottleFirstFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		TimeoutFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		DelayFrameSubscription(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		ToYieldInstruction(): UniRx.ObservableYieldInstruction<T>
		ToYieldInstruction(cancel: any): UniRx.ObservableYieldInstruction<T>
		ToYieldInstruction(throwOnError: boolean): UniRx.ObservableYieldInstruction<T>
		ToYieldInstruction(throwOnError: boolean, cancel: any): UniRx.ObservableYieldInstruction<T>
		ToAwaitableEnumerator(cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: T) => void), cancel: any): any
		ToAwaitableEnumerator(onError: ((obj: System.Exception) => void), cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: T) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(cancel: any): any
		StartAsCoroutine(onResult: ((obj: T) => void), cancel: any): any
		StartAsCoroutine(onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(onResult: ((obj: T) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		ObserveOnMainThread(): System.IObservable<T>
		ObserveOnMainThread(dispatchType: UniRx.MainThreadDispatchType): System.IObservable<T>
		SubscribeOnMainThread(): System.IObservable<T>
		TakeUntilDestroy(target: any): System.IObservable<T>
		TakeUntilDestroy(target: any): System.IObservable<T>
		TakeUntilDisable(target: any): System.IObservable<T>
		TakeUntilDisable(target: any): System.IObservable<T>
		RepeatUntilDestroy(target: any): System.IObservable<T>
		RepeatUntilDestroy(target: any): System.IObservable<T>
		RepeatUntilDisable(target: any): System.IObservable<T>
		RepeatUntilDisable(target: any): System.IObservable<T>
		FrameInterval(): System.IObservable<UniRx.FrameInterval<T>>
		FrameTimeInterval(ignoreTimeScale: boolean): System.IObservable<UniRx.TimeInterval<T>>
		BatchFrame(): System.IObservable<any>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<any>
		BatchFrame(): System.IObservable<UniRx.Unit>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Unit>
		Subscribe(): System.IDisposable
		Subscribe(onNext: ((obj: T) => void)): System.IDisposable
		Subscribe(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void)): System.IDisposable
		Subscribe(onNext: ((obj: T) => void), onCompleted: (() => void)): System.IDisposable
		Subscribe(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		IsRequiredSubscribeOnCurrentThread(): boolean
		IsRequiredSubscribeOnCurrentThread(scheduler: UniRx.IScheduler): boolean
		ToTask(): any
		ToTask(state: System.Object): any
		ToTask(cancellationToken: any): any
		ToTask(cancellationToken: any, state: System.Object): any
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand<T>
		BindToButtonOnClick(button: any, onClick: ((obj: UniRx.Unit) => void), initialValue: boolean): System.IDisposable
		ToReactiveProperty(): UniRx.IReadOnlyReactiveProperty<T>
		ToReactiveProperty(initialValue: T): UniRx.IReadOnlyReactiveProperty<T>
		ToReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<T>
		ToSequentialReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<T>
		ToReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<T>
		ToSequentialReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<T>
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any, selector: ((arg: T) => string)): System.IDisposable
		SubscribeToInteractable(selectable: any): System.IDisposable
		LogToUnityDebug(): System.IDisposable
		Debug(label: string): System.IObservable<T>
		Debug(logger: UniRx.Diagnostics.Logger): System.IObservable<T>
		Scan(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Scan(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate), resultSelector: ((arg: TAccumulate) => TResult)): System.IObservable<TResult>
		GetAwaiter(): UniRx.AsyncSubject<TSource>
		GetAwaiter(cancellationToken: any): UniRx.AsyncSubject<TSource>
		Multicast(subject: UniRx.ISubject<T>): UniRx.IConnectableObservable<T>
		Publish(): UniRx.IConnectableObservable<T>
		Publish(initialValue: T): UniRx.IConnectableObservable<T>
		PublishLast(): UniRx.IConnectableObservable<T>
		Replay(): UniRx.IConnectableObservable<T>
		Replay(scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		Replay(bufferSize: number): UniRx.IConnectableObservable<T>
		Replay(bufferSize: number, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		Replay(window: any): UniRx.IConnectableObservable<T>
		Replay(window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		Replay(bufferSize: number, window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		Share(): System.IObservable<T>
		Wait(): T
		Wait(timeout: any): T
		Concat(): System.IObservable<TSource>
		Concat(...seconds: System.IObservable<TSource>[]): System.IObservable<TSource>
		Merge(...seconds: System.IObservable<T>[]): System.IObservable<T>
		Merge(second: System.IObservable<T>, scheduler: UniRx.IScheduler): System.IObservable<T>
		Merge(): System.IObservable<T>
		Merge(maxConcurrent: number): System.IObservable<T>
		Zip(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		CombineLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		ZipLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		Switch(): System.IObservable<T>
		WithLatestFrom(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		StartWith(value: T): System.IObservable<T>
		StartWith(valueFactory: (() => T)): System.IObservable<T>
		StartWith(...values: T[]): System.IObservable<T>
		StartWith(values: any): System.IObservable<T>
		StartWith(scheduler: UniRx.IScheduler, value: T): System.IObservable<T>
		StartWith(scheduler: UniRx.IScheduler, values: any): System.IObservable<T>
		StartWith(scheduler: UniRx.IScheduler, ...values: T[]): System.IObservable<T>
		Synchronize(): System.IObservable<T>
		Synchronize(gate: System.Object): System.IObservable<T>
		ObserveOn(scheduler: UniRx.IScheduler): System.IObservable<T>
		SubscribeOn(scheduler: UniRx.IScheduler): System.IObservable<T>
		DelaySubscription(dueTime: any): System.IObservable<T>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		DelaySubscription(dueTime: any): System.IObservable<T>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		Amb(second: System.IObservable<T>): System.IObservable<T>
		AsObservable(): System.IObservable<T>
		Cast(): System.IObservable<TResult>
		Cast(witness: TResult): System.IObservable<TResult>
		OfType(): System.IObservable<TResult>
		OfType(witness: TResult): System.IObservable<TResult>
		AsUnitObservable(): System.IObservable<UniRx.Unit>
		AsSingleUnitObservable(): System.IObservable<UniRx.Unit>
		Repeat(): System.IObservable<T>
		RepeatSafe(): System.IObservable<T>
		Select(selector: ((arg: T) => TR)): System.IObservable<TR>
		Select(selector: ((arg1: T, arg2: number) => TR)): System.IObservable<TR>
		Where(predicate: ((arg: T) => boolean)): System.IObservable<T>
		Where(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<T>
		ContinueWith(other: System.IObservable<TR>): System.IObservable<TR>
		ContinueWith(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(other: System.IObservable<TR>): System.IObservable<TR>
		SelectMany(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(selector: ((arg1: TSource, arg2: number) => System.IObservable<TResult>)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: T) => System.IObservable<TC>), resultSelector: ((arg1: T, arg2: TC) => TR)): System.IObservable<TR>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => System.IObservable<TCollection>), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		SelectMany(selector: ((arg: TSource) => any)): System.IObservable<TResult>
		SelectMany(selector: ((arg1: TSource, arg2: number) => any)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: TSource) => any), resultSelector: ((arg1: TSource, arg2: TCollection) => TResult)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => any), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		ToArray(): System.IObservable<T[]>
		ToList(): System.IObservable<any>
		Do(observer: System.IObserver<T>): System.IObservable<T>
		Do(onNext: ((obj: T) => void)): System.IObservable<T>
		Do(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void)): System.IObservable<T>
		Do(onNext: ((obj: T) => void), onCompleted: (() => void)): System.IObservable<T>
		Do(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IObservable<T>
		DoOnError(onError: ((obj: System.Exception) => void)): System.IObservable<T>
		DoOnCompleted(onCompleted: (() => void)): System.IObservable<T>
		DoOnTerminate(onTerminate: (() => void)): System.IObservable<T>
		DoOnSubscribe(onSubscribe: (() => void)): System.IObservable<T>
		DoOnCancel(onCancel: (() => void)): System.IObservable<T>
		Materialize(): System.IObservable<UniRx.Notification<T>>
		Dematerialize(): System.IObservable<T>
		DefaultIfEmpty(): System.IObservable<T>
		DefaultIfEmpty(defaultValue: T): System.IObservable<T>
		Distinct(): System.IObservable<TSource>
		Distinct(comparer: any): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey)): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<TSource>
		DistinctUntilChanged(): System.IObservable<T>
		DistinctUntilChanged(comparer: any): System.IObservable<T>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey)): System.IObservable<T>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey), comparer: any): System.IObservable<T>
		IgnoreElements(): System.IObservable<T>
		ForEachAsync(onNext: ((obj: T) => void)): System.IObservable<UniRx.Unit>
		ForEachAsync(onNext: ((arg1: T, arg2: number) => void)): System.IObservable<UniRx.Unit>
		Finally(finallyAction: (() => void)): System.IObservable<T>
		Catch(errorHandler: ((arg: TException) => System.IObservable<T>)): System.IObservable<T>
		CatchIgnore(): System.IObservable<TSource>
		CatchIgnore(errorAction: ((obj: TException) => void)): System.IObservable<TSource>
		Retry(): System.IObservable<TSource>
		Retry(retryCount: number): System.IObservable<TSource>
		OnErrorRetry(): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void)): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any, delayScheduler: UniRx.IScheduler): System.IObservable<TSource>
		Take(count: number): System.IObservable<T>
		Take(duration: any): System.IObservable<T>
		Take(duration: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		TakeWhile(predicate: ((arg: T) => boolean)): System.IObservable<T>
		TakeWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<T>
		TakeUntil(other: System.IObservable<TOther>): System.IObservable<T>
		TakeLast(count: number): System.IObservable<T>
		TakeLast(duration: any): System.IObservable<T>
		TakeLast(duration: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		Skip(count: number): System.IObservable<T>
		Skip(duration: any): System.IObservable<T>
		Skip(duration: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		SkipWhile(predicate: ((arg: T) => boolean)): System.IObservable<T>
		SkipWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<T>
		SkipUntil(other: System.IObservable<TOther>): System.IObservable<T>
		Buffer(count: number): System.IObservable<any>
		Buffer(count: number, skip: number): System.IObservable<any>
		Buffer(timeSpan: any): System.IObservable<any>
		Buffer(timeSpan: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, count: number): System.IObservable<any>
		Buffer(timeSpan: any, count: number, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(windowBoundaries: System.IObservable<TWindowBoundary>): System.IObservable<any>
		Pairwise(): System.IObservable<UniRx.Pair<T>>
		Pairwise(selector: ((arg1: T, arg2: T) => TR)): System.IObservable<TR>
		Last(): System.IObservable<T>
		Last(predicate: ((arg: T) => boolean)): System.IObservable<T>
		LastOrDefault(): System.IObservable<T>
		LastOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<T>
		First(): System.IObservable<T>
		First(predicate: ((arg: T) => boolean)): System.IObservable<T>
		FirstOrDefault(): System.IObservable<T>
		FirstOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<T>
		Single(): System.IObservable<T>
		Single(predicate: ((arg: T) => boolean)): System.IObservable<T>
		SingleOrDefault(): System.IObservable<T>
		SingleOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<T>
		GroupBy(keySelector: ((arg: TSource) => TKey)): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement)): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		Timestamp(): System.IObservable<UniRx.Timestamped<TSource>>
		Timestamp(scheduler: UniRx.IScheduler): System.IObservable<UniRx.Timestamped<TSource>>
		TimeInterval(): System.IObservable<UniRx.TimeInterval<TSource>>
		TimeInterval(scheduler: UniRx.IScheduler): System.IObservable<UniRx.TimeInterval<TSource>>
		Delay(dueTime: any): System.IObservable<T>
		Delay(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Sample(interval: any): System.IObservable<T>
		Sample(interval: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		Throttle(dueTime: any): System.IObservable<TSource>
		Throttle(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		ThrottleFirst(dueTime: any): System.IObservable<TSource>
		ThrottleFirst(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Timeout(dueTime: any): System.IObservable<T>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		Timeout(dueTime: any): System.IObservable<T>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		SelectMany(coroutine: any, publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: (() => any), publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: ((arg: T) => any)): System.IObservable<UniRx.Unit>
		DelayFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		Sample(sampler: System.IObservable<T2>): System.IObservable<T>
		SampleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		ThrottleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		ThrottleFirstFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		TimeoutFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		DelayFrameSubscription(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		ToYieldInstruction(): UniRx.ObservableYieldInstruction<T>
		ToYieldInstruction(cancel: any): UniRx.ObservableYieldInstruction<T>
		ToYieldInstruction(throwOnError: boolean): UniRx.ObservableYieldInstruction<T>
		ToYieldInstruction(throwOnError: boolean, cancel: any): UniRx.ObservableYieldInstruction<T>
		ToAwaitableEnumerator(cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: T) => void), cancel: any): any
		ToAwaitableEnumerator(onError: ((obj: System.Exception) => void), cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: T) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(cancel: any): any
		StartAsCoroutine(onResult: ((obj: T) => void), cancel: any): any
		StartAsCoroutine(onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(onResult: ((obj: T) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		ObserveOnMainThread(): System.IObservable<T>
		ObserveOnMainThread(dispatchType: UniRx.MainThreadDispatchType): System.IObservable<T>
		SubscribeOnMainThread(): System.IObservable<T>
		TakeUntilDestroy(target: any): System.IObservable<T>
		TakeUntilDestroy(target: any): System.IObservable<T>
		TakeUntilDisable(target: any): System.IObservable<T>
		TakeUntilDisable(target: any): System.IObservable<T>
		RepeatUntilDestroy(target: any): System.IObservable<T>
		RepeatUntilDestroy(target: any): System.IObservable<T>
		RepeatUntilDisable(target: any): System.IObservable<T>
		RepeatUntilDisable(target: any): System.IObservable<T>
		FrameInterval(): System.IObservable<UniRx.FrameInterval<T>>
		FrameTimeInterval(ignoreTimeScale: boolean): System.IObservable<UniRx.TimeInterval<T>>
		BatchFrame(): System.IObservable<any>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<any>
		BatchFrame(): System.IObservable<UniRx.Unit>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Unit>
		Subscribe(): System.IDisposable
		Subscribe(onNext: ((obj: T) => void)): System.IDisposable
		Subscribe(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void)): System.IDisposable
		Subscribe(onNext: ((obj: T) => void), onCompleted: (() => void)): System.IDisposable
		Subscribe(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		IsRequiredSubscribeOnCurrentThread(): boolean
		IsRequiredSubscribeOnCurrentThread(scheduler: UniRx.IScheduler): boolean
		ToTask(): any
		ToTask(state: System.Object): any
		ToTask(cancellationToken: any): any
		ToTask(cancellationToken: any, state: System.Object): any
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand<T>
		BindToButtonOnClick(button: any, onClick: ((obj: UniRx.Unit) => void), initialValue: boolean): System.IDisposable
		ToReactiveProperty(): UniRx.IReadOnlyReactiveProperty<T>
		ToReactiveProperty(initialValue: T): UniRx.IReadOnlyReactiveProperty<T>
		ToReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<T>
		ToSequentialReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<T>
		ToReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<T>
		ToSequentialReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<T>
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any, selector: ((arg: T) => string)): System.IDisposable
		SubscribeToInteractable(selectable: any): System.IDisposable
		LogToUnityDebug(): System.IDisposable
		Debug(label: string): System.IObservable<T>
		Debug(logger: UniRx.Diagnostics.Logger): System.IObservable<T>
	}
}
