// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.EventPattern`1
declare namespace UniRx {
	class EventPattern<TEventArgs> extends UniRx.EventPattern<System.Object, TEventArgs> implements UniRx.IEventPattern<System.Object, TEventArgs> {
		constructor(sender: System.Object, e: TEventArgs)
	}
}
