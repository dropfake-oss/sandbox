// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.FrameCountType
declare namespace UniRx {
	enum FrameCountType {
		Update = 0,
		FixedUpdate = 1,
		EndOfFrame = 2,
	}
}
