// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: mscorlib
// Type: System.Object
declare namespace System {
	class Object {
		constructor()
		Equals(obj: System.Object): boolean
		static Equals(objA: System.Object, objB: System.Object): boolean
		GetHashCode(): number
		GetType(): any
		ToString(): string
		static ReferenceEquals(objA: System.Object, objB: System.Object): boolean
		HasValueByReflection(propertyName: string): boolean
		GetValueByReflection(propertyName: string): System.Object
		SetValueByReflection(propertyName: string, value: System.Object): void
		CallBoolMethodByReflection(methodName: string): boolean
		ToJson(): string
		Clone(fallbackCloner: any, tryPreserveInstances: boolean): System.Object
		CloneViaFakeSerialization(): System.Object
		IsConvertibleTo(type: any, guaranteed: boolean): boolean
		IsConvertibleTo(guaranteed: boolean): boolean
		ConvertTo(type: any): System.Object
		ConvertTo(): T
		ToShortString(maxLength: number): string
		Serialize(forceReflected: boolean): any
		IsUnityNull(): boolean
		ToSafeString(): string
		Analyser(context: any): any
		Analyser(context: any): TAnalyser
		Analysis(context: any): any
		Analysis(context: any): TAnalysis
		Analyser(reference: any): any
		Analyser(reference: any): TAnalyser
		Analysis(reference: any): any
		Analysis(reference: any): TAnalysis
		Describe(): void
		HasDescriptor(): boolean
		Descriptor(): any
		Descriptor(): TDescriptor
		Description(): any
		Description(): TDescription
		ConvertTo(): T
		TryConvertTo(result?: any): boolean
	}
}
