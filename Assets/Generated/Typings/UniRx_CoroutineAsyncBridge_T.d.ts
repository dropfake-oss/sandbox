// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.CoroutineAsyncBridge`1
declare namespace UniRx {
	class CoroutineAsyncBridge<T> extends System.Object {
		readonly IsCompleted: boolean
		static Start(awaitTarget: T): UniRx.CoroutineAsyncBridge<T>
		OnCompleted(continuation: (() => void)): void
		GetResult(): T
	}
}
