// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Notification`1
declare namespace UniRx {
	abstract class Notification<T> extends System.Object {
		readonly Value: T
		readonly HasValue: boolean
		readonly Exception: System.Exception
		readonly Kind: UniRx.NotificationKind
		Equals(other: UniRx.Notification<T>): boolean
		Equals(obj: System.Object): boolean
		Accept(observer: System.IObserver<T>): void
		Accept<TResult>(observer: UniRx.IObserver<T, TResult>): TResult
		Accept(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): void
		Accept<TResult>(onNext: ((arg: T) => TResult), onError: ((arg: System.Exception) => TResult), onCompleted: (() => TResult)): TResult
		ToObservable(): System.IObservable<T>
		ToObservable(scheduler: UniRx.IScheduler): System.IObservable<T>
	}
}
