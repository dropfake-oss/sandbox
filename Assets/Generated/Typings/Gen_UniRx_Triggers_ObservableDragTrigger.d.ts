// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableDragTrigger
declare namespace UniRx.Triggers {
	class ObservableDragTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnDragAsObservable(): System.IObservable<any>
	}
}
