// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.OptimizedObservableExtensions
declare namespace UniRx {
	abstract class OptimizedObservableExtensions extends System.Object {
		static IsRequiredSubscribeOnCurrentThread<T>(source: System.IObservable<T>): boolean
		static IsRequiredSubscribeOnCurrentThread<T>(source: System.IObservable<T>, scheduler: UniRx.IScheduler): boolean
	}
}
