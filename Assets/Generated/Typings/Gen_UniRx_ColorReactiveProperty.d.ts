// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ColorReactiveProperty
declare namespace UniRx {
	class ColorReactiveProperty extends UniRx.ReactiveProperty<UnityEngine.Color> implements System.IObservable<UnityEngine.Color>, UniRx.IReadOnlyReactiveProperty<UnityEngine.Color>, UniRx.IReactiveProperty<UnityEngine.Color>, UniRx.IOptimizedObservable<UnityEngine.Color>, System.IDisposable {
		constructor()
		constructor(initialValue: UnityEngine.Color)
	}
}
