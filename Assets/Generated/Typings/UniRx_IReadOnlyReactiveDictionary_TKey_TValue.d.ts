// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IReadOnlyReactiveDictionary`2
declare namespace UniRx {
	interface IReadOnlyReactiveDictionary<TKey, TValue> {
		readonly Count: number
		ContainsKey(key: TKey): boolean
		TryGetValue(key: TKey, value: any): boolean
		ObserveAdd(): System.IObservable<UniRx.DictionaryAddEvent<TKey, TValue>>
		ObserveCountChanged(notifyCurrentCount: boolean): System.IObservable<number>
		ObserveRemove(): System.IObservable<UniRx.DictionaryRemoveEvent<TKey, TValue>>
		ObserveReplace(): System.IObservable<UniRx.DictionaryReplaceEvent<TKey, TValue>>
		ObserveReset(): System.IObservable<UniRx.Unit>
	}
}
