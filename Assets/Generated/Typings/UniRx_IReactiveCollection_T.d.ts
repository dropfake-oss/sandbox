// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IReactiveCollection`1
declare namespace UniRx {
	interface IReactiveCollection<T> extends UniRx.IReadOnlyReactiveCollection<T> {
		readonly Count: number
		Move(oldIndex: number, newIndex: number): void
	}
}
