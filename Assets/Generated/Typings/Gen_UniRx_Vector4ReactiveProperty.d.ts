// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Vector4ReactiveProperty
declare namespace UniRx {
	class Vector4ReactiveProperty extends UniRx.ReactiveProperty<UnityEngine.Vector4> implements System.IObservable<UnityEngine.Vector4>, UniRx.IReadOnlyReactiveProperty<UnityEngine.Vector4>, UniRx.IReactiveProperty<UnityEngine.Vector4>, UniRx.IOptimizedObservable<UnityEngine.Vector4>, System.IDisposable {
		constructor()
		constructor(initialValue: UnityEngine.Vector4)
	}
}
