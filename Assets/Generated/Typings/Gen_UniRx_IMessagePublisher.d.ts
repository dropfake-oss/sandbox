// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IMessagePublisher
declare namespace UniRx {
	interface IMessagePublisher {
		Publish<T>(message: T): void
	}
}
