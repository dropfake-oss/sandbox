// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Timestamped`1
declare namespace UniRx {
	class Timestamped<T> extends System.Object {
		readonly Value: T
		readonly Timestamp: any
		constructor(value: T, timestamp: any)
		Equals(other: UniRx.Timestamped<T>): boolean
		Equals(obj: System.Object): boolean
		GetHashCode(): number
		ToString(): string
	}
}
