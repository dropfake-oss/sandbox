// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservablePointerEnterTrigger
declare namespace UniRx.Triggers {
	class ObservablePointerEnterTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnPointerEnterAsObservable(): System.IObservable<any>
	}
}
