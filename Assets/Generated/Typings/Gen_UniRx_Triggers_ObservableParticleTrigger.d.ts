// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableParticleTrigger
declare namespace UniRx.Triggers {
	class ObservableParticleTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnParticleCollisionAsObservable(): System.IObservable<any>
		OnParticleTriggerAsObservable(): System.IObservable<UniRx.Unit>
	}
}
