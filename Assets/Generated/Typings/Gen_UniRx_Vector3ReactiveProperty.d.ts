// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Vector3ReactiveProperty
declare namespace UniRx {
	class Vector3ReactiveProperty extends UniRx.ReactiveProperty<UnityEngine.Vector3> implements System.IObservable<UnityEngine.Vector3>, UniRx.IReadOnlyReactiveProperty<UnityEngine.Vector3>, UniRx.IReactiveProperty<UnityEngine.Vector3>, UniRx.IOptimizedObservable<UnityEngine.Vector3>, System.IDisposable {
		constructor()
		constructor(initialValue: UnityEngine.Vector3)
	}
}
