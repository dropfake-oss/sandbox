// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IntReactiveProperty
declare namespace UniRx {
	class IntReactiveProperty extends UniRx.ReactiveProperty<number> implements System.IObservable<number>, UniRx.IReadOnlyReactiveProperty<number>, UniRx.IReactiveProperty<number>, UniRx.IOptimizedObservable<number>, System.IDisposable {
		constructor()
		constructor(initialValue: number)
	}
}
