// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Observer
declare namespace UniRx {
	abstract class Observer extends System.Object {
		static Create<T>(onNext: ((obj: T) => void)): System.IObserver<T>
		static Create<T>(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void)): System.IObserver<T>
		static Create<T>(onNext: ((obj: T) => void), onCompleted: (() => void)): System.IObserver<T>
		static Create<T>(onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IObserver<T>
		static CreateAutoDetachObserver<T>(observer: System.IObserver<T>, disposable: System.IDisposable): System.IObserver<T>
	}
}
