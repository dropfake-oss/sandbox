// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.AsyncReactiveCommand`1
declare namespace UniRx {
	class AsyncReactiveCommand<T> extends System.Object implements UniRx.IAsyncReactiveCommand<T> {
		readonly CanExecute: UniRx.IReadOnlyReactiveProperty<boolean>
		readonly IsDisposed: boolean
		constructor()
		constructor(canExecuteSource: System.IObservable<boolean>)
		constructor(sharedCanExecute: UniRx.IReactiveProperty<boolean>)
		Execute(parameter: T): System.IDisposable
		Subscribe(asyncAction: ((arg: T) => System.IObservable<UniRx.Unit>)): System.IDisposable
		Dispose(): void
		WaitUntilExecuteAsync(cancellationToken: any): any
		GetAwaiter(): any
		BindTo(button: any): System.IDisposable
		BindToOnClick(button: any, asyncOnClick: ((arg: UniRx.Unit) => System.IObservable<UniRx.Unit>)): System.IDisposable
	}
}
