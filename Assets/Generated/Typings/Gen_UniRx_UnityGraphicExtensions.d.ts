// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.UnityGraphicExtensions
declare namespace UniRx {
	abstract class UnityGraphicExtensions extends System.Object {
		static DirtyLayoutCallbackAsObservable(graphic: any): System.IObservable<UniRx.Unit>
		static DirtyMaterialCallbackAsObservable(graphic: any): System.IObservable<UniRx.Unit>
		static DirtyVerticesCallbackAsObservable(graphic: any): System.IObservable<UniRx.Unit>
	}
}
