// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableMouseTrigger
declare namespace UniRx.Triggers {
	class ObservableMouseTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnMouseDownAsObservable(): System.IObservable<UniRx.Unit>
		OnMouseDragAsObservable(): System.IObservable<UniRx.Unit>
		OnMouseEnterAsObservable(): System.IObservable<UniRx.Unit>
		OnMouseExitAsObservable(): System.IObservable<UniRx.Unit>
		OnMouseOverAsObservable(): System.IObservable<UniRx.Unit>
		OnMouseUpAsObservable(): System.IObservable<UniRx.Unit>
		OnMouseUpAsButtonAsObservable(): System.IObservable<UniRx.Unit>
	}
}
