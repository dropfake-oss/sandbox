// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UnityEngine.CoreModule
// Type: UnityEngine.Vector3
declare namespace UnityEngine {
	class Vector3 extends Array {
		static readonly kEpsilon: number
		static readonly kEpsilonNormalSqrt: number
		/** X component of the vector.
		 */
		x: number
		/** Y component of the vector.
		 */
		y: number
		/** Z component of the vector.
		 */
		z: number
		/** Returns this vector with a magnitude of 1 (Read Only).
		 */
		readonly normalized: UnityEngine.Vector3
		/** Returns the length of this vector (Read Only).
		 */
		readonly magnitude: number
		/** Returns the squared length of this vector (Read Only).
		 */
		readonly sqrMagnitude: number
		/** Shorthand for writing Vector3(0, 0, 0).
		 */
		static readonly zero: UnityEngine.Vector3
		/** Shorthand for writing Vector3(1, 1, 1).
		 */
		static readonly one: UnityEngine.Vector3
		/** Shorthand for writing Vector3(0, 0, 1).
		 */
		static readonly forward: UnityEngine.Vector3
		/** Shorthand for writing Vector3(0, 0, -1).
		 */
		static readonly back: UnityEngine.Vector3
		/** Shorthand for writing Vector3(0, 1, 0).
		 */
		static readonly up: UnityEngine.Vector3
		/** Shorthand for writing Vector3(0, -1, 0).
		 */
		static readonly down: UnityEngine.Vector3
		/** Shorthand for writing Vector3(-1, 0, 0).
		 */
		static readonly left: UnityEngine.Vector3
		/** Shorthand for writing Vector3(1, 0, 0).
		 */
		static readonly right: UnityEngine.Vector3
		/** Shorthand for writing Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity).
		 */
		static readonly positiveInfinity: UnityEngine.Vector3
		/** Shorthand for writing Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity).
		 */
		static readonly negativeInfinity: UnityEngine.Vector3
		constructor(x: number, y: number, z: number)
		constructor(x: number, y: number)
		/** Spherically interpolates between two vectors.
		 */
		static Slerp(a: UnityEngine.Vector3, b: UnityEngine.Vector3, t: number): UnityEngine.Vector3
		/** Spherically interpolates between two vectors.
		 */
		static SlerpUnclamped(a: UnityEngine.Vector3, b: UnityEngine.Vector3, t: number): UnityEngine.Vector3
		/** Makes vectors normalized and orthogonal to each other.
		 */
		static OrthoNormalize(normal: any, tangent: any): void
		/** Makes vectors normalized and orthogonal to each other.
		 */
		static OrthoNormalize(normal: any, tangent: any, binormal: any): void
		/** Rotates a vector current towards target.
		 * @param current The vector being managed.
		 * @param target The vector.
		 * @param maxRadiansDelta The maximum angle in radians allowed for this rotation.
		 * @param maxMagnitudeDelta The maximum allowed change in vector magnitude for this rotation.
		 * @returns The location that RotateTowards generates. 
		 */
		static RotateTowards(current: UnityEngine.Vector3, target: UnityEngine.Vector3, maxRadiansDelta: number, maxMagnitudeDelta: number): UnityEngine.Vector3
		/** Linearly interpolates between two points.
		 * @param a Start value, returned when t = 0.
		 * @param b End value, returned when t = 1.
		 * @param t Value used to interpolate between a and b.
		 * @returns Interpolated value, equals to a + (b - a) * t. 
		 */
		static Lerp(a: UnityEngine.Vector3, b: UnityEngine.Vector3, t: number): UnityEngine.Vector3
		/** Linearly interpolates between two vectors.
		 */
		static LerpUnclamped(a: UnityEngine.Vector3, b: UnityEngine.Vector3, t: number): UnityEngine.Vector3
		/** Calculate a position between the points specified by current and target, moving no farther than the distance specified by maxDistanceDelta.
		 * @param current The position to move from.
		 * @param target The position to move towards.
		 * @param maxDistanceDelta Distance to move current per call.
		 * @returns The new position. 
		 */
		static MoveTowards(current: UnityEngine.Vector3, target: UnityEngine.Vector3, maxDistanceDelta: number): UnityEngine.Vector3
		/** Gradually changes a vector towards a desired goal over time.
		 * @param current The current position.
		 * @param target The position we are trying to reach.
		 * @param currentVelocity The current velocity, this value is modified by the function every time you call it.
		 * @param smoothTime Approximately the time it will take to reach the target. A smaller value will reach the target faster.
		 * @param maxSpeed Optionally allows you to clamp the maximum speed.
		 * @param deltaTime The time since the last call to this function. By default Time.deltaTime.
		 */
		static SmoothDamp(current: UnityEngine.Vector3, target: UnityEngine.Vector3, currentVelocity: any, smoothTime: number, maxSpeed: number): UnityEngine.Vector3
		/** Gradually changes a vector towards a desired goal over time.
		 * @param current The current position.
		 * @param target The position we are trying to reach.
		 * @param currentVelocity The current velocity, this value is modified by the function every time you call it.
		 * @param smoothTime Approximately the time it will take to reach the target. A smaller value will reach the target faster.
		 * @param maxSpeed Optionally allows you to clamp the maximum speed.
		 * @param deltaTime The time since the last call to this function. By default Time.deltaTime.
		 */
		static SmoothDamp(current: UnityEngine.Vector3, target: UnityEngine.Vector3, currentVelocity: any, smoothTime: number): UnityEngine.Vector3
		/** Gradually changes a vector towards a desired goal over time.
		 * @param current The current position.
		 * @param target The position we are trying to reach.
		 * @param currentVelocity The current velocity, this value is modified by the function every time you call it.
		 * @param smoothTime Approximately the time it will take to reach the target. A smaller value will reach the target faster.
		 * @param maxSpeed Optionally allows you to clamp the maximum speed.
		 * @param deltaTime The time since the last call to this function. By default Time.deltaTime.
		 */
		static SmoothDamp(current: UnityEngine.Vector3, target: UnityEngine.Vector3, currentVelocity: any, smoothTime: number, maxSpeed: number, deltaTime: number): UnityEngine.Vector3
		/** Set x, y and z components of an existing Vector3.
		 */
		Set(newX: number, newY: number, newZ: number): void
		/** Multiplies two vectors component-wise.
		 */
		static Scale(a: UnityEngine.Vector3, b: UnityEngine.Vector3): UnityEngine.Vector3
		/** Multiplies every component of this vector by the same component of scale.
		 */
		Scale(scale: UnityEngine.Vector3): void
		/** Cross Product of two vectors.
		 */
		static Cross(lhs: UnityEngine.Vector3, rhs: UnityEngine.Vector3): UnityEngine.Vector3
		GetHashCode(): number
		/** Returns true if the given vector is exactly equal to this vector.
		 */
		Equals(other: System.Object): boolean
		Equals(other: UnityEngine.Vector3): boolean
		/** Reflects a vector off the plane defined by a normal.
		 */
		static Reflect(inDirection: UnityEngine.Vector3, inNormal: UnityEngine.Vector3): UnityEngine.Vector3
		/** Makes this vector have a magnitude of 1.
		 */
		static Normalize(value: UnityEngine.Vector3): UnityEngine.Vector3
		Normalize(): void
		/** Dot Product of two vectors.
		 */
		static Dot(lhs: UnityEngine.Vector3, rhs: UnityEngine.Vector3): number
		/** Projects a vector onto another vector.
		 */
		static Project(vector: UnityEngine.Vector3, onNormal: UnityEngine.Vector3): UnityEngine.Vector3
		/** Projects a vector onto a plane defined by a normal orthogonal to the plane.
		 * @param planeNormal The direction from the vector towards the plane.
		 * @param vector The location of the vector above the plane.
		 * @returns The location of the vector on the plane. 
		 */
		static ProjectOnPlane(vector: UnityEngine.Vector3, planeNormal: UnityEngine.Vector3): UnityEngine.Vector3
		/** Calculates the angle between vectors from and.
		 * @param from The vector from which the angular difference is measured.
		 * @param to The vector to which the angular difference is measured.
		 * @returns The angle in degrees between the two vectors. 
		 */
		static Angle(from: UnityEngine.Vector3, to: UnityEngine.Vector3): number
		/** Calculates the signed angle between vectors from and to in relation to axis.
		 * @param from The vector from which the angular difference is measured.
		 * @param to The vector to which the angular difference is measured.
		 * @param axis A vector around which the other vectors are rotated.
		 * @returns Returns the signed angle between from and to in degrees. 
		 */
		static SignedAngle(from: UnityEngine.Vector3, to: UnityEngine.Vector3, axis: UnityEngine.Vector3): number
		/** Returns the distance between a and b.
		 */
		static Distance(a: UnityEngine.Vector3, b: UnityEngine.Vector3): number
		/** Returns a copy of vector with its magnitude clamped to maxLength.
		 */
		static ClampMagnitude(vector: UnityEngine.Vector3, maxLength: number): UnityEngine.Vector3
		static Magnitude(vector: UnityEngine.Vector3): number
		static SqrMagnitude(vector: UnityEngine.Vector3): number
		/** Returns a vector that is made from the smallest components of two vectors.
		 */
		static Min(lhs: UnityEngine.Vector3, rhs: UnityEngine.Vector3): UnityEngine.Vector3
		/** Returns a vector that is made from the largest components of two vectors.
		 */
		static Max(lhs: UnityEngine.Vector3, rhs: UnityEngine.Vector3): UnityEngine.Vector3
		ToString(): string
		/** Returns a formatted string for this vector.
		 * @param format A numeric format string.
		 * @param formatProvider An object that specifies culture-specific formatting.
		 */
		ToString(format: string): string
		/** Returns a formatted string for this vector.
		 * @param format A numeric format string.
		 * @param formatProvider An object that specifies culture-specific formatting.
		 */
		ToString(format: string, formatProvider: any): string
		Compare(v2: UnityEngine.Vector3, accuracy: number): boolean
	}
}
