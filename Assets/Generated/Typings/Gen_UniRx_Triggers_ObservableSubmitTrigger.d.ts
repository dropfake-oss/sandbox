// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableSubmitTrigger
declare namespace UniRx.Triggers {
	class ObservableSubmitTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnSubmitAsObservable(): System.IObservable<any>
	}
}
