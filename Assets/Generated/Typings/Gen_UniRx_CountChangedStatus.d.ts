// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.CountChangedStatus
declare namespace UniRx {
	enum CountChangedStatus {
		Increment = 0,
		Decrement = 1,
		Empty = 2,
		Max = 3,
	}
}
